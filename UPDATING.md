20191020:
* add the following lines to settings.py:
   * `FRONTEND_TICKET_URL = 'https://s.nfvpinball.nl/9c/%d'`
   * `FRONTEND_PLAYER_URL = 'https://s.nfvpinball.nl/9p/%d'`
   * `FRONTEND_MAIN_GAME_URL = 'https://s.nfvpinball.nl/9mg/%d'`
   * `FRONTEND_CLASSICS_GAME_URL = 'https://s.nfvpinball.nl/9cg/%d'`

20190927:
* add the following line to the `JWT_AUTH` dict in settings.py:
   * `'JWT_PAYLOAD_HANDLER': 'util.jwt_payload_handler.jwt_payload_handler',`

20190612:
* added `IFPA_PLAYER_IMAGE_URL` to `backend/backend/settings.py.example`
   * add to your local `settings.py`
 

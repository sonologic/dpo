from django.conf import settings
from django.db import transaction
from django.db.models import Subquery, IntegerField, OuterRef, Count, F, Sum
from django.db.models.functions import Coalesce
from rest_framework import viewsets, serializers, status, mixins
from rest_framework.exceptions import APIException
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from classics.views import InvalidParameterException
from main.models import Game, PlayerScore, GameRanking
from main.renderers import ApronCardRenderer
from main.serializers import GameSerializer, PlayerScoreSerializer, RankingSerializer, \
    GameRankingSerializer, PlayerSerializer
from player.models import Player
from util.point_calculation import rank_to_points


class InvalidInputException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Bad request"


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all().order_by('number')
    serializer_class = GameSerializer


class GameRankingViewSet(viewsets.ModelViewSet):
    queryset = GameRanking.objects.all()
    serializer_class = GameRankingSerializer

    @staticmethod
    def update_game_ranking(request, game):
        """
        Updates cached ranking (GameRanking objects) for the specified game.

        Removes all GameRanking objects for the specified game, calculates the ranking of all
        scores for the game and stores the new ranking in the database.

        :param request: django request
        :param game: classics.Game object

        """
        with transaction.atomic():
            GameRanking.objects.filter(game=game).delete()
            higher_scores = PlayerScore.objects.filter(valid=True).filter(game=OuterRef('game')).filter(
                score__gt=OuterRef('score')).order_by().values('game')
            count_higher_scores = higher_scores.annotate(c=Count('*')).values('c')

            ranked = PlayerScore.objects.filter(game=game).filter(valid=True).annotate(
                rank=Coalesce(Subquery(count_higher_scores, output_field=IntegerField()), 0)).annotate(
                rank=F('rank') + 1).order_by('rank')

            num_players = len(ranked)

            ranked_data = map(
                lambda x: {
                    'score': reverse('playerscore-detail', args=[x['id']], request=request),
                    'game': reverse('game-detail', args=[x['game_id']], request=request),
                    'rank': x['rank'],
                    'points': rank_to_points(x['rank'], num_players)
                },
                ranked.values()
            )
            serializer = GameRankingSerializer(data=list(ranked_data), many=True, context={'request': request})
            serializer.is_valid(raise_exception=True)
            serializer.save()


class RankingRefreshView(APIView):
    def get(self, request, format=None):
        games = Game.objects.all()
        for game in games:
            GameRankingViewSet.update_game_ranking(request, game)
        return Response({'detail': 'refreshed'})


class PlayerViewSet(viewsets.ModelViewSet):
    """
    ViewSet for tickets.

    Serializer includes the score entries for the ticket.

    Writing to the view updates the embedded scores for each score that has a
    game and score defined. If one of them is undefined, the score will silently
    be dropped.

    After updating a score, the ranking for the game the score is for will also
    be updated through GameRankingViewSet.update_game_ranking.
    """
    serializer_class = PlayerSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        if partial:
            raise NotImplementedError("partial updates not supported")

        instance = self.get_object()

        if 'scores' in request.data:
            self.update_scores(request, instance)

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def update_scores(self, request, player):
        """ Save new scores, ignore existing ones
        """
        for score in request.data['scores']:
            if score['id'] is None:
                self.create_score_if_valid(request, score, player)

    def create_score_if_valid(self, request, data, player):
        if data['score'] is None and data['game'] is None:
            return

        serializer = PlayerScoreSerializer(data=data)
        if not serializer.is_valid():
            raise serializers.ValidationError(serializer.errors)

        if serializer.validated_data['player'].pk is not player.pk:
            raise InvalidInputException("cannot set scores from other player")

        invalidated_game = None

        with transaction.atomic():
            if serializer.validated_data['wildcard']:
                self.__validate_wildcard(request, serializer, player)
                invalid_score = self.__get_score_to_invalidate_by_wildcard(request, serializer, player)
                invalid_score.valid = False
                invalidated_game = invalid_score.game
                invalid_score.save()

            serializer.save()

        GameRankingViewSet.update_game_ranking(request, serializer.validated_data['game'])
        if invalidated_game is not None and invalidated_game.id != serializer.validated_data['game'].id:
            GameRankingViewSet.update_game_ranking(request, invalidated_game)

    def __validate_wildcard(self, _, serializer, player):
        has_wildcards = PlayerScore.objects.filter(player=player).filter(wildcard=True).exists()
        if has_wildcards:
            raise InvalidInputException("player already used wildcard")

    def __get_score_to_invalidate_by_wildcard(self, request, serializer, player):
        """ Get player score that is invalidated by the wildcard

        :param request: request
        :param serializer: the wildcard data
        :param player: the player

        :return: the score instance to invalidate
        """
        count_scores = PlayerScore.objects.filter(player=player).count()

        score = PlayerScore.objects.filter(player=player).filter(game=serializer.validated_data['game']).first()

        if count_scores < settings.MAIN_TOURNAMENT['SCORES_PER_PLAYER']:
            if score is None:
                raise InvalidInputException(
                    "when not all games played, wildcard is only allowed on already played game")
        elif score is None:
            points = GameRanking.objects.filter(score=OuterRef('id')).values('points')
            rank = GameRanking.objects.filter(score=OuterRef('id')).values('rank')

            score = PlayerScore.objects.filter(player=player).\
                annotate(points=Subquery(points)).\
                annotate(rank=Subquery(rank)).\
                order_by('points', 'rank', 'id').\
                first()

        return score

    def get_queryset(self):
        order_field = self.request.GET.get('order', 'tag')
        if order_field not in ['id', '-id', 'points', '-points', 'tag', '-tag']:
            raise InvalidParameterException(detail="invalid order requested")

        return Player.objects.filter(in_main_tournament=True).annotate(
            points=Coalesce(Sum('scores__ranking__points'), 0)).order_by(order_field, 'number', 'full_name')


class PlayerByNumberView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, number, format=None):
        ticket = get_object_or_404(Player, id=number)

        serializer = PlayerSerializer(ticket, context={'request': request})

        return Response(serializer.data)


class PlayerScoreViewSet(viewsets.ModelViewSet):
    queryset = PlayerScore.objects.filter(player__in_main_tournament=True)
    serializer_class = PlayerScoreSerializer
    permission_classes = (IsAdminUser,) # player scores should be updated through PlayerViewSet

    def perform_create(self, serializer):
        serializer.save()  # may need to do some extra verification
        GameRankingViewSet.update_game_ranking(self.request, serializer.validated_data['game'])

    def perform_update(self, serializer):
        serializer.save()  # may need to do some extra verification
        GameRankingViewSet.update_game_ranking(self.request, serializer.validated_data['game'])

    def perform_destroy(self, instance):
        game = GameRanking.objects.get(score=instance).game
        GameRanking.objects.filter(score=instance).delete()
        instance.delete()
        GameRankingViewSet.update_game_ranking(self.request, game)


class RankingView(APIView):
    def get(self, request, game_id=None, format=None):
        games = Game.objects.all().order_by('number')

        if game_id is not None:
            games = games.filter(id=game_id)

        result = []

        for game in games:
            ranked = GameRanking.objects.filter(game=game).select_related(
                'score', 'score__player')

            game_serializer = GameSerializer(game, context={'request': request})
            ranking_serializer = RankingSerializer(ranked, many=True, context={'request': request})

            result.append({
                'game': game_serializer.data,
                'ranked': ranking_serializer.data
            })

        if game_id is not None:
            result = result[0]

        return Response(result)


class ApronCardView(mixins.ListModelMixin, GenericViewSet):
    permission_classes = [IsAdminUser]
    queryset = Game.objects.all().order_by('number')
    serializer_class = GameSerializer
    renderer_classes = [ApronCardRenderer]

# Generated by Django 2.1.1 on 2018-10-08 18:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('player', '0003_auto_20180915_1445'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('number', models.PositiveSmallIntegerField()),
                ('valid', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='GameRanking',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False, verbose_name='ID')),
                ('rank', models.IntegerField()),
                ('points', models.IntegerField()),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='scores', to='main.Game')),
            ],
            options={
                'ordering': ['game_id', 'rank'],
            },
        ),
        migrations.CreateModel(
            name='PlayerScore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.SmallIntegerField()),
                ('score', models.BigIntegerField()),
                ('valid', models.BooleanField(default=True)),
                ('wildcard', models.BooleanField(default=False)),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='main.Game')),
                ('player', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='main_scores', to='player.Player')),
            ],
            options={
                'ordering': ['player_id', 'position'],
            },
        ),
        migrations.AddField(
            model_name='gameranking',
            name='score',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='ranking', to='main.PlayerScore'),
        ),
        migrations.AlterUniqueTogether(
            name='playerscore',
            unique_together={('player', 'position'), ('player', 'game')},
        ),
    ]

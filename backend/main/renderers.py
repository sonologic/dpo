import os

from util.pdf_renderer import PdfRenderer
from django.conf import settings


class ApronCardRenderer(PdfRenderer):
    template_page_name = 'template_apron_card_page.tex'
    template_item_name = 'template_apron_card.tex'

    @staticmethod
    def _render_item(template, game):
        game['tournament'] = 'MAIN TOURNAMENT'
        game['url'] = settings.FRONTEND_MAIN_GAME_URL % (game['id'])
        return template.render(game)

    @staticmethod
    def _graphics_path():
        return os.path.join(os.path.dirname(__file__), 'template_images')

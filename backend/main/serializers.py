from django.conf import settings
from rest_framework import serializers

import player.serializers
from main.models import Game, PlayerScore, GameRanking
from player.models import Player
from util.serializer_fields import ReadOnlyRelatedField


class GameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Game
        fields = ('id', 'url', 'title', 'number', 'valid')


class PlayerScoreSerializer(serializers.HyperlinkedModelSerializer):
    rank = ReadOnlyRelatedField(source='ranking.rank', read_only=True)
    points = ReadOnlyRelatedField(source='ranking.points', read_only=True)

    class Meta:
        model = PlayerScore
        fields = ('id', 'url', 'player', 'position', 'game', 'score', 'wildcard', 'valid', 'rank', 'points')

    def validate(self, data):
        if data['position'] < 1:
            raise serializers.ValidationError("position smaller than 1")

        max_position = settings.MAIN_TOURNAMENT['SCORES_PER_PLAYER'] + 1

        if data['position'] > max_position:
            raise serializers.ValidationError("position greater than {}".format(max_position))
        if data['position'] is max_position and not data['wildcard']:
            raise serializers.ValidationError("score at highest position must be wildcard")

        return data

    def create(self, validated_data):
        return PlayerScore.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.score = validated_data['score']
        instance.game = validated_data['game']
        instance.save()
        return instance


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer that shows a player with their scores
    """
    scores = PlayerScoreSerializer(many=True, read_only=True)
    points = serializers.IntegerField(read_only=True)

    class Meta:
        model = Player
        fields = (
            'id', 'url', 'number', 'full_name', 'country_code',
            'tag', 'wppr_rank', 'image', 'points', 'scores',
            'ifpa_info', 'activated')


class RankingSerializer(serializers.Serializer):
    score = serializers.IntegerField(source='score.score')
    rank = serializers.IntegerField()
    points = serializers.IntegerField()
    player = player.serializers.PlayerSerializer(source='score.player')

    def update(self, instance, validated_data):
        raise NotImplementedError()

    def create(self, validated_data):
        raise NotImplementedError()


class GameRankingSerializer(serializers.HyperlinkedModelSerializer):
    points = serializers.IntegerField()
    score_value = serializers.IntegerField(read_only=True, source='score.score')

    class Meta:
        model = GameRanking
        fields = ('id', 'url', 'game', 'game_id', 'score', 'score_value', 'rank', 'points')

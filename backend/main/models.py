from django.db import models

from player.models import Player


class Game(models.Model):
    title = models.CharField(max_length=255)
    number = models.PositiveSmallIntegerField()
    valid = models.BooleanField(default=True)

    def __str__(self):
        return "{number}: {title}".format(number=self.number, title=self.title)


class PlayerScore(models.Model):
    """
    Player score entry.

    Records a score on a particular game for a particular entry (position) on a ticket.

    Field valid is set to True unless the score is voided by the wildcard.
    Wildcard is set to False except for the wildcard score (if any).
    """
    player = models.ForeignKey(Player, on_delete=models.PROTECT, related_name='scores')
    position = models.SmallIntegerField()
    game = models.ForeignKey(Game, on_delete=models.PROTECT, null=False)
    score = models.BigIntegerField(null=False)
    valid = models.BooleanField(default=True)
    wildcard = models.BooleanField(default=False)

    class Meta:
        unique_together = (('player', 'position'), ('player', 'game', 'wildcard'))
        ordering = ['player_id', 'position']


class GameRanking(models.Model):
    """
    Cache for score ranking per game.

    For each score, stores the game, relative ranking to all other scores for that game and the
    number of points for that score relative to all other scores for the game.
    """
    id = models.BigAutoField(primary_key=True, serialize=False, verbose_name='ID')
    game = models.ForeignKey(Game, on_delete=models.PROTECT, related_name='scores')
    score = models.OneToOneField(PlayerScore, on_delete=models.PROTECT, related_name='ranking')
    rank = models.IntegerField(null=False)
    points = models.IntegerField(null=False)

    class Meta:
        ordering = ['game_id', 'rank']

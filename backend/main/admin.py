from django.contrib import admin

from main.models import Game, GameRanking
from main.models import PlayerScore


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('number', 'title',)
    ordering = ('number',)


@admin.register(PlayerScore)
class TicketScoreAdmin(admin.ModelAdmin):
    list_display = ('player', 'position', 'game', 'score', 'wildcard', 'valid')
    ordering = ('player', 'position')

@admin.register(GameRanking)
class GameRankingAdmin(admin.ModelAdmin):
    list_display = ('id', 'game', 'score', 'rank', 'points')
    ordering = ('game', 'rank')

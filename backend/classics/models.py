from django.db import models

from player.models import Player


class Game(models.Model):
    title = models.CharField(max_length=255)
    number = models.PositiveSmallIntegerField()
    valid = models.BooleanField(default=True)

    def __str__(self):
        return "{number}: {title}".format(number=self.number, title=self.title)


class Ticket(models.Model):
    number = models.IntegerField(unique=True)
    player = models.ForeignKey(Player, on_delete=models.PROTECT)
    valid = models.BooleanField(default=False)
    void = models.BooleanField(default=False)

    def __str__(self):
        return "{number} - {player}".format(number=self.number, player=self.player.full_name)


class TicketScore(models.Model):
    """
    Ticket score entry.

    Records a score on a particular game for a particular entry (position) on a ticket.
    """
    ticket = models.ForeignKey(Ticket, on_delete=models.PROTECT, related_name='scores')
    position = models.SmallIntegerField()
    game = models.ForeignKey(Game, on_delete=models.PROTECT, null=False)
    score = models.BigIntegerField(null=False)

    class Meta:
        unique_together = (('ticket', 'position'), ('ticket', 'game'),)
        ordering = ['ticket_id', 'position']


class GameRanking(models.Model):
    """
    Cache for score ranking per game.

    For each score, stores the game, relative ranking to all other scores for that game and the
    number of points for that score relative to all other scores for the game.
    """
    id = models.BigAutoField(primary_key=True, serialize=False, verbose_name='ID')
    game = models.ForeignKey(Game, on_delete=models.PROTECT, related_name='scores')
    score = models.ForeignKey(TicketScore, on_delete=models.PROTECT, related_name='ranking')
    rank = models.IntegerField(null=False)
    points = models.IntegerField(null=False)

    class Meta:
        ordering = ['game_id', 'rank']

from django.apps import AppConfig


class ClassicsConfig(AppConfig):
    name = 'classics'

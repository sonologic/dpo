import os

from django.conf import settings

from util.pdf_renderer import PdfRenderer


class TicketRenderer(PdfRenderer):
    template_page_name = 'template_tickets_page.tex'
    template_item_name = 'template_ticket.tex'

    @staticmethod
    def _render_item(template, ticket):
        ticket['url'] = settings.FRONTEND_TICKET_URL % (ticket['id'])
        ticket['number'] = 123
        return template.render(ticket)

    @staticmethod
    def _graphics_path():
        return os.path.join(os.path.dirname(__file__), 'template_images')


class ApronCardRenderer(PdfRenderer):
    template_page_name = 'template_apron_card_page.tex'
    template_item_name = 'template_apron_card.tex'

    @staticmethod
    def _render_item(template, game):
        game['tournament'] = 'CLASSICS'
        game['url'] = settings.FRONTEND_CLASSICS_GAME_URL % (game['id'])
        return template.render(game)

    @staticmethod
    def _graphics_path():
        return os.path.join(os.path.dirname(__file__), 'template_images')

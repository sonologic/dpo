# Generated by Django 2.1.1 on 2018-10-17 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classics', '0014_auto_20180930_1930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='number',
            field=models.IntegerField(unique=True),
        ),
    ]

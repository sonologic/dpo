# Generated by Django 2.1.1 on 2018-09-30 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classics', '0007_auto_20180930_1424'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='wildcard_allowed',
            field=models.BooleanField(default=True),
        ),
    ]

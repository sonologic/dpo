from django.conf import settings
from rest_framework import serializers

from classics.models import Game, Ticket, TicketScore, GameRanking
from player.serializers import PlayerSerializer


class GameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Game
        fields = ('id', 'url', 'title', 'number',)


class TicketScoreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TicketScore
        fields = ('id', 'url', 'ticket', 'position', 'game', 'score')

    def validate_position(self, value):
        if value < 1:
            raise serializers.ValidationError("position smaller than 1")
        if value > settings.CLASSICS['SCORES_PER_TICKET']:
            raise serializers.ValidationError("position greater than {}".format(settings.CLASSICS['SCORES_PER_TICKET']))
        return value

    def create(self, validated_data):
        return TicketScore.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.score = validated_data['score']
        instance.game = validated_data['game']
        instance.save()
        return instance


class TicketSerializer(serializers.HyperlinkedModelSerializer):
    points = serializers.IntegerField(read_only=True)
    scores = TicketScoreSerializer(many=True, read_only=True)
    player_details = PlayerSerializer(read_only=True, source='player')

    class Meta:
        model = Ticket
        fields = ('id', 'url', 'number', 'valid', 'void', 'player', 'player_details', 'points', 'scores')


class RankingSerializer(serializers.Serializer):
    score = serializers.IntegerField(source='score.score')
    rank = serializers.IntegerField()
    points = serializers.IntegerField()
    ticket = serializers.HyperlinkedRelatedField(view_name='ticket-detail', read_only=True, source='score.ticket')
    player = PlayerSerializer(source='score.ticket.player')

    def update(self, instance, validated_data):
        raise NotImplementedError()

    def create(self, validated_data):
        raise NotImplementedError()


class GameRankingSerializer(serializers.HyperlinkedModelSerializer):
    points = serializers.IntegerField()
    score_value = serializers.IntegerField(read_only=True, source='score.score')

    class Meta:
        model = GameRanking
        fields = ('id', 'url', 'game', 'game_id', 'score', 'score_value', 'rank', 'points')

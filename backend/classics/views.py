from django.conf import settings
from django.db import transaction
from django.db.models import Subquery, IntegerField, OuterRef, Count, F, Sum
from django.db.models.functions import Coalesce
from rest_framework import viewsets, serializers, status, mixins
from rest_framework.exceptions import APIException
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from classics.models import Game, Ticket, TicketScore, GameRanking
from classics.renderers import TicketRenderer, ApronCardRenderer
from classics.serializers import GameSerializer, TicketSerializer, TicketScoreSerializer, RankingSerializer, \
    GameRankingSerializer
from util.point_calculation import rank_to_points


class InvalidParameterException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Bad request"


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all().order_by('number')
    serializer_class = GameSerializer


class GameRankingViewSet(viewsets.ModelViewSet):
    queryset = GameRanking.objects.all()
    serializer_class = GameRankingSerializer

    @staticmethod
    def update_game_ranking(request, game):
        """
        Updates cached ranking (GameRanking objects) for the specified game.

        Removes all GameRanking objects for the specified game, calculates the ranking of all
        scores for the game and stores the new ranking in the database.

        :param request: django request
        :param game: classics.Game object

        """
        with transaction.atomic():
            GameRanking.objects.filter(game=game).delete()

            higher_scores = TicketScore.objects.filter(game=OuterRef('game')).\
                filter(ticket__valid=True).filter(ticket__void=False).\
                filter(score__gt=OuterRef('score')).order_by().values('game')

            count_higher_scores = higher_scores.annotate(c=Count('*')).values('c')

            ranked = TicketScore.objects.filter(game=game).filter(ticket__valid=True).filter(ticket__void=False).\
                annotate(rank=Coalesce(Subquery(count_higher_scores, output_field=IntegerField()), 0)).\
                annotate(rank=F('rank') + 1).\
                order_by('rank')

            num_players = len(ranked)

            ranked_data = map(
                lambda x: {
                    'score': reverse('ticketscore-detail', args=[x['id']], request=request),
                    'game': reverse('game-detail', args=[x['game_id']], request=request),
                    'rank': x['rank'],
                    'points': rank_to_points(x['rank'], num_players)
                },
                ranked.values()
            )
            serializer = GameRankingSerializer(data=list(ranked_data), many=True, context={'request': request})
            serializer.is_valid(raise_exception=True)
            serializer.save()


class RankingRefreshView(APIView):
    def get(self, request, format=None):
        games = Game.objects.all()
        for game in games:
            GameRankingViewSet.update_game_ranking(request, game)
        return Response({'detail': 'refreshed'})


class TicketViewSet(viewsets.ModelViewSet):
    """
    ViewSet for tickets.

    Serializer includes the score entries for the ticket.

    Writing to the view updates the embedded scores for each score that has a
    game and score defined. If one of them is undefined, the score will silently
    be dropped.

    After updating a score, the ranking for the game the score is for will also
    be updated through GameRankingViewSet.update_game_ranking.
    """
    serializer_class = TicketSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        if partial:
            raise NotImplementedError("partial updates not supported")

        self.update_scores(request)

        instance = self.get_object()
        num_scores = TicketScore.objects.filter(ticket=instance).count()

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['valid'] = num_scores is settings.CLASSICS['SCORES_PER_TICKET']

        refresh_ranking = self.__must_refresh_ranking(serializer, instance)

        self.perform_update(serializer)

        if refresh_ranking:
            self.__refresh_ranking(request, instance)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        instance = Ticket.objects.annotate(points=Sum('scores__ranking__points')).get(pk=instance.pk)
        serializer = self.get_serializer(instance)

        return Response(serializer.data)

    def update_scores(self, request):
        """ Save new scores, ignore existing ones
        """
        if 'scores' in request.data:
            for score in request.data['scores']:
                if score['id'] is None:
                    self.create_score_if_valid(request, score)

    @staticmethod
    def __must_refresh_ranking(serializer, instance):
        refresh_ranking = False
        if serializer.validated_data['valid'] is not instance.valid:
            refresh_ranking = True
        if serializer.validated_data['void'] is not instance.void:
            refresh_ranking = True
        return refresh_ranking

    @staticmethod
    def __refresh_ranking(request, instance):
        scores = TicketScore.objects.filter(ticket=instance)
        for score in scores:
            GameRankingViewSet.update_game_ranking(request, score.game)

    def create_score_if_valid(self, request, data):
        if data['score'] is None and data['game'] is None:
            return

        serializer = TicketScoreSerializer(data=data)
        if not serializer.is_valid():
            raise serializers.ValidationError(serializer.errors)
        serializer.save()

    def get_queryset(self):
        order_field = self.request.GET.get('order', 'number')
        filter_valid = self.request.GET.get('valid', None)

        if order_field not in ['number', '-number', 'points', '-points']:
            raise InvalidParameterException(detail="invalid order requested")

        tickets = Ticket.objects.annotate(points=Sum('scores__ranking__points'))

        if filter_valid is not None:
            tickets = tickets.filter(valid=True).filter(void=False)
        
        return tickets.order_by(order_field, 'number')


class TicketByNumberView(APIView):
    def get(self, request, number, format=None):
        ticket = get_object_or_404(Ticket.objects.annotate(points=Sum('scores__ranking__points')), number=number)
        serializer = TicketSerializer(ticket, context={'request': request})
        return Response(serializer.data)


class TicketsByPlayerView(APIView):
    def get(self, request, player_id, format=None):
        tickets = Ticket.objects.filter(player_id=player_id).annotate(points=Sum('scores__ranking__points'))
        serializer = TicketSerializer(tickets, context={'request': request}, many=True)
        return Response(serializer.data)


class TicketScoreViewSet(viewsets.ModelViewSet):
    queryset = TicketScore.objects.all()
    serializer_class = TicketScoreSerializer
    permission_classes = (IsAdminUser,) # scores should be updated through TicketViewSet

    def perform_create(self, serializer):
        serializer.save()
        GameRankingViewSet.update_game_ranking(self.request, serializer.validated_data['game'])

    def perform_update(self, serializer):
        serializer.save()
        GameRankingViewSet.update_game_ranking(self.request, serializer.validated_data['game'])


class RankingView(APIView):
    def get(self, request, id=None, format=None):

        games = Game.objects.all().order_by('number')

        if id is not None:
            games = games.filter(id=id)

        result = []

        for game in games:
            ranked = GameRanking.objects.filter(game=game).select_related(
                'score', 'score__ticket', 'score__ticket__player')

            game_serializer = GameSerializer(game, context={'request': request})
            ranking_serializer = RankingSerializer(ranked, many=True, context={'request': request})

            result.append({
                'game': game_serializer.data,
                'ranked': ranking_serializer.data
            })

        if id is not None:
            result = result[0]

        return Response(result)


class TicketView(mixins.ListModelMixin, GenericViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = TicketSerializer
    renderer_classes = [TicketRenderer]

    def get_queryset(self):
        if 'from' not in self.request.query_params:
            raise InvalidParameterException('from parameter missing')
        if 'to' not in self.request.query_params:
            raise InvalidParameterException('to parameter missing')
        return [Ticket(pk=x) for x in range(int(self.request.query_params['from']),
                                            int(self.request.query_params['to']))]


class ApronCardView(mixins.ListModelMixin, GenericViewSet):
    permission_classes = [IsAdminUser]
    queryset = Game.objects.all().order_by('number')
    serializer_class = GameSerializer
    renderer_classes = [ApronCardRenderer]

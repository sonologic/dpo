from django.contrib import admin

from classics.models import Game, Ticket, TicketScore, GameRanking


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('number', 'title',)
    ordering = ('number',)


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('number', 'player', 'valid', 'void')
    ordering = ('number',)


@admin.register(TicketScore)
class TicketScoreAdmin(admin.ModelAdmin):
    list_display = ('ticket', 'position', 'game', 'score')
    ordering = ('ticket', 'position')


@admin.register(GameRanking)
class GameRankingAdmin(admin.ModelAdmin):
    list_display = ('game', 'rank', 'score', 'points')
    ordering = ('game', 'rank')

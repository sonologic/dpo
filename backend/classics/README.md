Ranking
=======

The query ranks all scores for a game. If two scores are the same,
they will be ranked at the same position,  with the subsequent position
skipped.

For example, say for one game we have scores:

* 100
* 50
* 50
* 20
* 10

The ranking will be:

* 100 -> 1
* 50 -> 2
* 50 -> 2
* 20 -> 4
* 10 -> 5
"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

import classics.views
import main.views
import player.views

router = routers.DefaultRouter()
router.register(r'players', player.views.PlayerViewSet)
router.register(r'classics/games', classics.views.GameViewSet)
router.register(r'classics/ticket', classics.views.TicketViewSet, basename='ticket')
router.register(r'classics/score', classics.views.TicketScoreViewSet)
router.register(r'classics/gameranking', classics.views.GameRankingViewSet)
router.register(r'main/games', main.views.GameViewSet)
router.register(r'main/player', main.views.PlayerViewSet, basename='player')
router.register(r'main/score', main.views.PlayerScoreViewSet)
router.register(r'main/gameranking', main.views.GameRankingViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^jwt/auth/$', obtain_jwt_token),
    url(r'^jwt/refresh/$', refresh_jwt_token),
    url(r'^classics/ranking/$', classics.views.RankingView.as_view()),
    url(r'^classics/ranking/(?P<id>.+)/$', classics.views.RankingView.as_view()),
    url(r'^classics/refresh-ranking/$', classics.views.RankingRefreshView.as_view()),
    url(r'^classics/ticket/number/(?P<number>.+)/$', classics.views.TicketByNumberView.as_view()),
    url(r'^classics/tickets/$', classics.views.TicketView.as_view({'get': 'list'}), name='classics-tickets'),
    url(r'^classics/aproncards/$', classics.views.ApronCardView.as_view({'get': 'list'}), name='classics-aproncards'),
    url(r'^main/ranking/$', main.views.RankingView.as_view()),
    url(r'^main/refresh-ranking/$', main.views.RankingRefreshView.as_view()),
    url(r'^main/ranking/(?P<game_id>.+)/$', main.views.RankingView.as_view()),
    url(r'^main/aproncards/$', main.views.ApronCardView.as_view({'get': 'list'}), name='main-aproncards'),
    url(r'^main/player/(?P<player_id>.+)/tickets/$', classics.views.TicketsByPlayerView.as_view(),
        name='player-tickets'),
    url(r'^player/badges/$', player.views.Badge.as_view({'get': 'list'}), name='player-badges'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    url('^schema$', get_schema_view(title="Example API")),
]


def rank_to_points(rank, num_players, max_points=100):
    """Convert ranking to points

    :param rank: rank (first result is 1)
    :param num_players: total number of players
    :param max_points: number of points to assign to best player
    :return: points
    """
    if rank > num_players:
        return 0
    return int(round((pow(num_players - rank + 1, 1.5) / pow(num_players, 1.5)) * max_points))

import codecs
import os
import subprocess
from tempfile import TemporaryDirectory

from django.template import loader
from rest_framework.renderers import BaseRenderer


class PdfRenderer(BaseRenderer):
    media_type = 'application/pdf'
    format = 'pdf'
    charset = None
    render_style = 'binary'

    template_page_name = 'template_page.tex'
    template_item_name = 'template_item.tex'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        page_template = loader.get_template(self.template_page_name)
        item_template = loader.get_template(self.template_item_name)
        with TemporaryDirectory() as render_dir:
            tex_file_name = os.path.join(render_dir, 'badges.tex')
            pdf_file_name = os.path.join(render_dir, 'badges.pdf')

            items = ""
            for item in data:
                items += self._render_item(item_template, item)

            context={
                'items': items,
                'graphics_path': self._graphics_path(),
                'curly_open': '{',
            }
            with codecs.open(tex_file_name, 'w', 'utf-8') as tex_file:
                tex_file.write(page_template.render(context=context))
            command_list = ['pdflatex', '-interaction', 'nonstopmode', '-halt-on-error', '-file-line-error', tex_file_name]
            pdflatex_result = subprocess.run(command_list, cwd=render_dir,
                                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if pdflatex_result.returncode != 0:
                raise Exception("pdflatex fail: %s" % (pdflatex_result.stdout))
            with open(pdf_file_name, 'rb') as pdf_file:
                return pdf_file.read()

    @staticmethod
    def _render_item(template, item):
        raise NotImplementedError("method should be implemented by subclass")

    @staticmethod
    def _graphics_path():
        raise NotImplementedError("method should be implemented by subclass")


def latex_escape(string):
    if type(string) is not str:
        return string

    replacements = {
        '~': '\\textasciitilde',
        '^': '\\textasciicircum',
        '\\': '\\\textbackslash'
    }
    for character in replacements:
        string = string.replace(character, replacements[character])
    for character in '&%$#_{}':
        string = string.replace(character, '\\'+character)
    return string

from rest_framework_jwt.utils import jwt_payload_handler as basic_jwt_payload_handler


def jwt_payload_handler(user):
    payload = basic_jwt_payload_handler(user)
    payload['is_staff'] = user.is_staff
    return payload

from rest_framework.relations import RelatedField


class ReadOnlyRelatedField(RelatedField):
    """
    A read only field that represents its targets using their
    value as the representation.
    """

    def __init__(self, **kwargs):
        kwargs['read_only'] = True
        super(ReadOnlyRelatedField, self).__init__(**kwargs)

    def to_representation(self, value):
        return value

""" Tools to query IFPA database"""
from json import JSONDecodeError

import requests
from django.conf import settings


def get_ifpa_info(ifpa_id):
    """ Retrieve ifpa info record from IFPA api

    """
    ifpa_player_url = settings.IFPA_PLAYER_URL.format(ifpa_id=ifpa_id, api_key=settings.IFPA_API_KEY)
    try:
        ifpa_info = requests.get(ifpa_player_url).json()
    except requests.exceptions.ConnectionError as e:
        return {'ifpa_connection_error': str(e)}
    except JSONDecodeError as e:
        return {'ifpa_json_decode_error': str(e)}
    return ifpa_info


def parse_initials(ifpa_info, default=""):
    initials = None
    if 'player' in ifpa_info:
        ifpa_info_player = ifpa_info['player']
        if 'initials' in ifpa_info_player:
            initials = ifpa_info_player['initials']
    if initials is None:
        initials = default
    return initials


def parse_country_code(ifpa_info, default=""):
    country_code = None
    if 'player' in ifpa_info:
        ifpa_info_player = ifpa_info['player']
        if 'country_code' in ifpa_info_player:
            country_code = ifpa_info_player['country_code']
    if country_code is None:
        country_code = default
    return country_code


def parse_wppr_rank(ifpa_info, default=None):
    wppr_rank = None
    if 'player_stats' in ifpa_info:
        ifpa_info_stats = ifpa_info['player_stats']
        if 'current_wppr_rank' in ifpa_info_stats:
            wppr_rank = ifpa_info_stats['current_wppr_rank']
    if wppr_rank is None:
        wppr_rank=default
    return wppr_rank


def player_image_url(ifpa_id):
    return settings.IFPA_PLAYER_IMAGE_URL.format(ifpa_id=ifpa_id)

Installation
============

Initialize a virtual environment:
```
python -m venv env
. ./env/bin/activate
pip install -r requirements.txt
```

Now copy `backend/settings.py.example` to `backend/settings.py`, and
edit it to suit your environment:

* Change the database settings under 'DATABASES'
** The database settings keyed `default` are used
** Only PostgreSQL is supported in production, in development you may get away with sqlite or mysql
* Change the `IFPA_API_KEY`
* Probably change the `SECRET_KEY` as well

Now start a development server:

```
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

This will run a development server on [https://localhost:8000/](https://localhost:8000/), with
an administration interface at [https://localhost:8000/admin](https://localhost:8000/admin) to which
you can log on with the super user created above.

Badge generator
===============

To use the badge generator, make sure pdflatex is available on the path and
has the [https://ctan.org/pkg/qrcode](qrcode package) installed.


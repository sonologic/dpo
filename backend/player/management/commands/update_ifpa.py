import random
import time

from django.core.management.base import BaseCommand, CommandError
from player.models import Player, update_ifpa_data


class Command(BaseCommand):
    help = 'Updates IFPA data for all players'

    def handle(self, *args, **options):
        for player in Player.objects.exclude(number=None):
            self.stdout.write('Updating player %s (%s)' % (player.number, player.full_name))
            update_ifpa_data(player)
            time.sleep(random.randint(3, 8))

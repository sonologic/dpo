from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import GenericViewSet

from player.models import Player
from player.renderers import BadgeRenderer
from player.serializers import PlayerSerializer


class PlayerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows players to be viewed or edited.
    """
    queryset = Player.objects.all().order_by('number', 'full_name')
    serializer_class = PlayerSerializer


class Badge(mixins.ListModelMixin, GenericViewSet):
    permission_classes = [IsAdminUser]
    queryset = Player.objects.all().order_by('number', 'full_name')
    serializer_class = PlayerSerializer
    renderer_classes = [BadgeRenderer]

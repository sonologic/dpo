import os

from django.conf import settings

from util.pdf_renderer import PdfRenderer, latex_escape


class BadgeRenderer(PdfRenderer):
    template_page_name = 'template_badges_page.tex'
    template_item_name = 'template_badge.tex'

    @staticmethod
    def _render_item(template, player):
        player['url'] = settings.FRONTEND_PLAYER_URL % (player['id'])
        player['ifpa_player_id'] = '-'
        player['ifpa_country_code'] = '--'
        player['ifpa_country_name'] = ''

        if player['ifpa_info'] is not None and 'player' in player['ifpa_info']:
            player['ifpa_player_id'] = player['ifpa_info']['player']['player_id']
            player['ifpa_country_code'] = player['ifpa_info']['player']['country_code']
            player['ifpa_country_name'] = player['ifpa_info']['player']['country_name']

        if player['ifpa_country_code'] is None:
            player['flag_code'] = '--'
        else:
            player['flag_code'] = player['ifpa_country_code'].lower()

        if player['ifpa_country_name'] is None:
            player['ifpa_country_name'] = ''
        if player['ifpa_player_id'] is None:
            player['ifpa_player_id'] = ''

        player = {key: latex_escape(value) for (key, value) in player.items()}

        player['curly_open'] = '{'

        return template.render(player)

    @staticmethod
    def _graphics_path():
        return os.path.join(os.path.dirname(__file__), 'template_images')

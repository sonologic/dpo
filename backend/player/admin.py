from django.contrib import admin

from player.models import Player


@admin.register(Player)
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'tag', 'full_name', 'country_code', 'in_main_tournament')
    ordering = ('number', 'tag', 'full_name')

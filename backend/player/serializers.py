from rest_framework import serializers

from player.models import Player


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    classics_tickets = serializers.HyperlinkedRelatedField(view_name='player-tickets', read_only=True)

    class Meta:
        model = Player
        fields = ('id', 'url', 'number', 'full_name', 'country_code', 'tag', 'ifpa_info', 'activated', 'image', 'classics_tickets')

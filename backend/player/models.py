from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from util.ifpa import get_ifpa_info, parse_initials, parse_wppr_rank, player_image_url, parse_country_code


class Player(models.Model):
    number = models.IntegerField(verbose_name="IFPA player number", default=0, null=True, blank=True)
    full_name = models.CharField(verbose_name="Full name", max_length=255)
    country_code = models.CharField(verbose_name="Country code",max_length=2, blank=True)
    tag = models.CharField(max_length=255, verbose_name="High score tag", blank=True)
    wppr_rank = models.IntegerField(verbose_name="World Pinball Player Ranking (WPPR)", null=True, blank=True)
    image = models.URLField(verbose_name="Player image", blank=True)
    in_main_tournament = models.BooleanField(default=True)
    ifpa_info = JSONField(null=True, blank=True)
    activated = models.BooleanField(verbose_name="Activated", default=True)

    class Meta:
        ordering = ['tag', 'full_name']

    def __str__(self):
        return "{tag} {name}".format(name=self.full_name, tag=self.tag)


@receiver(post_save, sender=Player)
def populate_ifpa_data(sender, **kwargs):
    """ Populate initials and wppr rank on player creation """
    if kwargs['update_fields'] is None and settings.IFPA_FETCH_ON_CREATE:
        player = kwargs['instance']
        update_ifpa_data(player)


def update_ifpa_data(player):
    ifpa_id = player.number
    if player.number:
        player.ifpa_info = get_ifpa_info(ifpa_id)
        player.tag = parse_initials(player.ifpa_info, '---')
        player.wppr_rank = parse_wppr_rank(player.ifpa_info)
        player.image = player_image_url(player.number)
        player.country_code = parse_country_code(player.ifpa_info, player.country_code)
        player.save(update_fields=['ifpa_info', 'tag', 'wppr_rank', 'country_code', 'image'])

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'http://localhost:8000/',
  //api_url: 'http://10.1.3.111:8000/',
  // api_url: 'http://192.168.1.79:8000/',
  jwt_whitelist: ['localhost:8000', '192.168.1.79:8000'],
  scores_per_ticket: 3,
  scores_per_player: 8,
  token_refresh_period_in_seconds: 1800,
  wallboard_refresh_period_in_seconds: 10,
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

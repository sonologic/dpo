export const environment = {
  production: true,
  api_url: 'https://scores.nfvpinball.nl/dpo2022/api/',
  jwt_whitelist: ['scores.nfvpinball.nl'],
  scores_per_ticket: 3,
  scores_per_player: 8,
  token_refresh_period_in_seconds: 1800,
  wallboard_refresh_period_in_seconds: 120,
};

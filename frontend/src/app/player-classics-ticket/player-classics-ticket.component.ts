import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from '../models/ticket';
import { Game } from '../models/game';

@Component({
  selector: 'app-player-classics-ticket',
  templateUrl: './player-classics-ticket.component.html',
  styleUrls: ['./player-classics-ticket.component.css']
})
export class PlayerClassicsTicketComponent implements OnInit {
  @Input() ticket: Ticket;
  @Input() games: Game[];

  constructor() { }

  ngOnInit() {
  }

  public getGameName(url: string): string {
    if (this.games) {
      let game = this.games.find(game => game.url == url);
      if (game) {
        return game.title;
      }
    }
  }
}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlayerClassicsTicketComponent } from './player-classics-ticket.component';

describe('PlayerClassicsTicketComponent', () => {
  let component: PlayerClassicsTicketComponent;
  let fixture: ComponentFixture<PlayerClassicsTicketComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerClassicsTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerClassicsTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

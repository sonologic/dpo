import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TicketScore } from '../models/ticket-score';
import { GamesService } from '../services/games.service';
import { Game } from '../models/game';
import { GenericScore } from '../models/generic-score';
import { formatNumber } from '@angular/common'

@Component({
  selector: 'app-score-entry',
  templateUrl: './score-entry.component.html',
  styleUrls: ['./score-entry.component.css']
})
export class ScoreEntryComponent implements OnInit {

  @Input() score: GenericScore;

  @Input() games: Game[];

  get validScore() {
    if (this.score.valid === undefined) {
      return true;
    }
    return this.score.valid;
  }

  constructor(private classicsGamesService: GamesService) { }

  ngOnInit() {
  }

  get diagnostic() { return JSON.stringify(this.score); }

  get scoreInput(): string {
    return formatNumber(this.score.score, 'en-EN');
  }
  set scoreInput(value: string) {
    this.score.score = parseInt(value.toString().replace(/\D/g, ''));
  }
}

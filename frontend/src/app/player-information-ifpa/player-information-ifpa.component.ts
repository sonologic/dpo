import { Component, OnInit, Input } from '@angular/core';
import { Ifpa_info } from '../models/ifpa_info';

@Component({
  selector: 'app-player-information-ifpa',
  templateUrl: './player-information-ifpa.component.html',
  styleUrls: ['./player-information-ifpa.component.css']
})
export class PlayerInformationIfpaComponent implements OnInit {
  @Input() display: 'vertical' | 'horizontal' = 'vertical';
  @Input() name: string;
  @Input() tag: string;
  @Input() imageUrl: string;
  @Input() ifpaInfo: Ifpa_info;

  constructor() { }

  ngOnInit() {
  }
}

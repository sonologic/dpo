import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GameRankingTableComponent } from './game-ranking-table.component';

describe('GameRankingTableComponent', () => {
  let component: GameRankingTableComponent;
  let fixture: ComponentFixture<GameRankingTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GameRankingTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameRankingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

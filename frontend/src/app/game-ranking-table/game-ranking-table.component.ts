import {Component, Input, OnInit} from '@angular/core';
import {GameRanking} from '../models/game_ranking';
import {GameRankingService} from '../services/game-ranking.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-game-ranking-table',
  templateUrl: './game-ranking-table.component.html',
  styleUrls: ['./game-ranking-table.component.css']
})
export class GameRankingTableComponent implements OnInit {

  gameRanking: GameRanking;
  @Input() tournament: string;

  constructor(
    private gameRankingService: GameRankingService
  ) { }

  ngOnInit() { }

  @Input() set game_id(id: number) {
    let observable: Observable<GameRanking>;

    if (this.tournament == 'classics') {
      observable = this.gameRankingService.getClassicsGameRanking(id);
    } else {
      observable = this.gameRankingService.getMainGameRanking(id);
    }

    observable.subscribe(
      gameRanking => this.gameRanking = gameRanking
    );
  }
}

import {Component, OnInit} from '@angular/core';
import {PlayersService} from '../services/players.service';
import {Player} from '../models/player';

@Component({
  selector: 'app-main-player',
  templateUrl: './main-player.component.html',
  styleUrls: ['./main-player.component.css']
})
export class MainPlayerComponent implements OnInit {

  players: Player[];

  selectedPlayer: number = null;

  constructor(
    private playersService: PlayersService
  ) { }

  ngOnInit() {
    this.playersService.getMainPlayers('tag').subscribe(
      players => this.players = players
    );
  }

}

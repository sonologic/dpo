import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainPlayerComponent } from './main-player.component';

describe('MainPlayerComponent', () => {
  let component: MainPlayerComponent;
  let fixture: ComponentFixture<MainPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*
 Union of PlayerScore and TicketScore, used as input to ScoreEntry component
 For a PlayerScore, wildcard/valid will make sense, ticket won't.
 For a TicketScore, wildcard/valid will be undefined (falsy), ticket will be valid.
 Not too happy with this, but it'll have to do. Possibly look into union types to refactor later.
 */

export class GenericScore {
  id: number;
  url: string;
  ticket: string;
  position: number;
  game: string;
  score: number;
  wildcard: boolean;
  valid: boolean;
}

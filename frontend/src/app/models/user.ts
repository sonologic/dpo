export class User {
  id: number;
  url: string;
  full_name: string;
  tag: string;
  is_staff: boolean;
}

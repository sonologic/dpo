import {TicketScore} from './ticket-score';
import {Player} from './player';

export class Ticket {
  id: number;
  url: string;
  valid: boolean;
  void: boolean;
  number: number;
  points: number;
  tournament: string;
  player: string;
  player_details: Player;
  scores: TicketScore[] = [];
}

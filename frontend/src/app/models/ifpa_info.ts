export class Ifpa_info {
    player: {
        age: number;
        city: string;
        state: string;
        gender: string;
        initials: string;
        last_name: string;
        player_id: string;
        first_name: string;
        country_code: string;
        country_name: string;
        excluded_flag: 'N' | 'Y';
        ifpa_registered: 'N' | 'Y';
    };
    player_stats: {
        best_finish: string;
        highest_rank: string;
        ratings_value: string;
        average_finish: string;
        last_year_rank: string;
        efficiency_rank: string;
        last_month_rank: string;
        efficiency_value: string;
        best_finish_count: string;
        current_wppr_rank: string;
        highest_rank_date: string;
        total_events_away: string;
        current_wppr_value: string;
        total_active_events: string;
        wppr_points_all_time: string;
        total_events_all_time: string;
        average_finish_last_year: string;
    };
}

export class Tournament {
  id: number;
  title: string;
  type: string;
  qualifiers: number;
  finals_url: string;

  static get tournaments(): Tournament[] {
    return [
      {
        id: 1,
        title: 'DPO 2022',
        type: 'main',
        qualifiers: 56,
        finals_url: 'https://s.nfvpinball.nl/dpo2022/mainfinals'
      },
      {
        id: 2,
        title: 'DPO 2022 Classics',
        type: 'classics',
        qualifiers: 16,
        finals_url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSiu2-p4meOFDkgr1gw_NLCiSgWNhZ7pAZsb6EGh7-N4uZMMti0aWZPBeYwIG8okS4_lrJ2i-XoD3wM/pubhtml'
      },
      {
        id: 3,
        title: 'DPO 2022 Ladies',
        type: 'ladies',
        qualifiers: 0,
        finals_url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTreVujWIHt7HQlenAIkEikL9xHXdr2ct3eT5x2diOXbGbxu6bKZXzu3xDcfzn2-7XeRCV-LKSkQPi7/pubhtml?gid=32537383&single=true'
      },
      {id: 4, title: 'DPO 2022 Swiss', type: 'swiss', qualifiers: 0, finals_url: 'https://s.nfvpinball.nl/dpo2022/swiss'},
    ];
  }

  static tournamentByType(type: string) {
    for (const tournament of Tournament.tournaments) {
      if (tournament.type === type) {
        return tournament;
      }
    }
    return null;
  }
}

import {Player} from './player';

export class PlayerScore {
  id: number;
  url: string;
  player: string;
  position: number;
  game: string;
  score: number;
  wildcard: boolean;
  valid: boolean;
}

export class MainPlayer extends Player {
  points: number;
  scores: PlayerScore[];
}

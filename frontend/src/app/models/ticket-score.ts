export class TicketScore {
  id: number;
  url: string;
  ticket: string;
  position: number;
  game: string;
  score: number;
}

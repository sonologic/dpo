export class Game {
  id: number;
  url: string;
  title: string;
  number: number;
}

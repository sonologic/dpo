import { Ifpa_info } from "./ifpa_info";

export class Player {
  id: number;
  url: string;
  number: number;
  full_name: string;
  country_code: string;
  tag: string;
  ifpa_info: Ifpa_info;
  activated: boolean;
  image: string;
}

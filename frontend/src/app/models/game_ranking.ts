import {Game} from './game';
import {Player} from './player';

export class GameRankingItem {
  score: number;
  rank: number;
  points: number;
  ticket: string;
  player: Player;
}

export class GameRanking {
  game: Game;
  ranked: GameRankingItem[];
}

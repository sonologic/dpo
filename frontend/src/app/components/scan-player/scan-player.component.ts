import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/app/models/player';

@Component({
  selector: 'app-scan-player',
  templateUrl: './scan-player.component.html',
  styleUrls: ['./scan-player.component.css']
})
export class ScanPlayerComponent implements OnInit {
  @Input() visible: boolean;
  @Output() onPlayerScanned: EventEmitter<Player> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public scanSuccessHandler($event: string){
    const indexPlayerId = $event.lastIndexOf('player/');
    if (indexPlayerId > -1) {
    } else {
      // todo message
      return;
    }

    const playerIdString = $event.substr(indexPlayerId + 7);
    const playerId = Number(playerIdString);
    // this.loadTicket(playerId);
  }

}

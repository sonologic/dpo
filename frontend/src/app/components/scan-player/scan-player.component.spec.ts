import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScanPlayerComponent } from './scan-player.component';

describe('ScanPlayerComponent', () => {
  let component: ScanPlayerComponent;
  let fixture: ComponentFixture<ScanPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

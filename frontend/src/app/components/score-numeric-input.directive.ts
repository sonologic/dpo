import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

import { formatNumber } from '@angular/common'

@Directive({
  selector: '[appScoreNumericInput]'
})
export class ScoreNumericInputDirective {

  constructor(public ngControl: NgControl) {

  }

  @HostListener('ngModelChange', ['$event']) onModelChange(event) {
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    this.onInputChange(event.target.value, true);
  }

  onInputChange(event, backspace) {
    if (event === undefined) {
      return;
    }
    let newVal = formatNumber(event.replace(/\D/g, ''), 'en-EN');
    this.ngControl.valueAccessor.writeValue(newVal);
  }
}

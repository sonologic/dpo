import { Component, Input, OnInit } from '@angular/core';
import { Player } from '../models/player';
import { MainPlayer, PlayerScore } from '../models/main_player';
import { PlayersService } from '../services/players.service';
import { environment } from '../../environments/environment';
import { Game } from '../models/game';
import { GamesService } from '../services/games.service';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-main-player-score',
  templateUrl: './main-player-score.component.html',
  styleUrls: ['./main-player-score.component.css']
})
export class MainPlayerScoreComponent implements OnInit {

  player_scores: MainPlayer;
  games: Game[];

  constructor(
    private notificationService: NotificationService,
    private playersService: PlayersService,
    private gamesService: GamesService
  ) { }

  ngOnInit() {
    this.gamesService.getMainGames().subscribe(
      games => this.games = games);
  }

  @Input()
  set player(player: Player) {
    this.playersService.getMainPlayerScores(player.id).subscribe(
      player_scores => this.processScores(player_scores));
  }

  processScores(mainPlayer: MainPlayer) {
    this.player_scores = mainPlayer;

    const scores = Array<PlayerScore>(environment.scores_per_player);

    const wildcardPosition = environment.scores_per_player + 1;

    for (let position = 1; position < environment.scores_per_player + 2; position++) {
      scores[position - 1] = {
        'id': null,
        'url': null,
        'game': null,
        'score': null,
        'position': position,
        'player': mainPlayer.url,
        'wildcard': position === wildcardPosition,
        'valid': true,
      };
    }

    for (const score of mainPlayer.scores) {
      scores[score.position - 1] = score;
    }

    mainPlayer.scores = scores;
    this.player_scores = mainPlayer;
  }


  onSubmit() {
    this.playersService.saveMainPlayer(this.player_scores)
      .subscribe(
        player_scores => this.processScores(player_scores),
        error => this.notificationService.apiError(error)
      );
  }
}
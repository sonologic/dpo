import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainPlayerScoreComponent } from './main-player-score.component';

describe('MainPlayerScoreComponent', () => {
  let component: MainPlayerScoreComponent;
  let fixture: ComponentFixture<MainPlayerScoreComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPlayerScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPlayerScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

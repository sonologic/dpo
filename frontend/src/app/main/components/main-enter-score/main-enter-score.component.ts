import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { formatNumber } from '@angular/common';
interface ScoreEntered {
  score: number;
  wildcard: boolean;
}
@Component({
  selector: 'app-main-enter-score',
  templateUrl: './main-enter-score.component.html',
  styleUrls: ['./main-enter-score.component.css']
})
export class MainEnterScoreComponent implements OnInit {
  @Input() visible: boolean;
  @Output() onScoreEntered: EventEmitter<ScoreEntered> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  @Output() onReset: EventEmitter<void> = new EventEmitter();

  private _score: string;
  public loading: boolean = false;
  public wildcard;
  public visualScore: number;
  public aggreed: boolean = false;

  public get score(): string {
    return this._score;
  }

  public set score(value: string) {
    if ((value === null)) {
      value = '';
    }
    this._score = formatNumber((<any>value).replace(/\D/g, ''), 'en-EN');
  }

  constructor() { }

  ngOnInit() {
  }

  public agree(): void {
    this.aggreed = !this.aggreed;
    this.visualScore = parseInt(this.score.toString().replace(/\D/g, ''));
  }

  public clean(): void {
    this.score = null;
    this.aggreed = false;
    this.visualScore = undefined;
    this.wildcard = false;
  }

  public submit(): void {
    let score = parseInt(this.score.toString().replace(/\D/g, ''));
    this.onScoreEntered.emit({ score: score, wildcard: this.wildcard });
  }
}

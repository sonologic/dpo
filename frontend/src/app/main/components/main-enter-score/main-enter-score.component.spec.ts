import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainEnterScoreComponent } from './main-enter-score.component';

describe('MainEnterScoreComponent', () => {
  let component: MainEnterScoreComponent;
  let fixture: ComponentFixture<MainEnterScoreComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainEnterScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainEnterScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

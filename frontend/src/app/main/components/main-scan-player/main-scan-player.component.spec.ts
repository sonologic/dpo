import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainScanPlayerComponent } from './main-scan-player.component';

describe('MainScanPlayerComponent', () => {
  let component: MainScanPlayerComponent;
  let fixture: ComponentFixture<MainScanPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainScanPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScanPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

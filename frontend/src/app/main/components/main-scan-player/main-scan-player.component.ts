import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MainPlayer } from 'src/app/models/main_player';
import { PlayersService } from 'src/app/services/players.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main-scan-player',
  templateUrl: './main-scan-player.component.html',
  styleUrls: ['./main-scan-player.component.css']
})
export class MainScanPlayerComponent implements OnInit {

  @Input() visible: boolean;
  @Input() type: 'scan' | 'manual';
  @Output() onPlayerFound: EventEmitter<MainPlayer> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  public loading = false;
  public searchTest: string;
  public playerList: MainPlayer[] = [];
  public playerListFiltered: MainPlayer[] = [];
  public selectedPlayer: MainPlayer;

  public get filter(): string {
    return this._filter;
  }
  public set filter(value: string) {
    this._filter = value;
    this.filterList(value);
  }

  private _filter: string;
  constructor(private playersService: PlayersService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loadPlayer();
  }

  private loadPlayer(): void {
    this.playersService
      .getMainPlayers()
      .subscribe((result) => {
        this.playerList = result;
        this.filter = '';
      });
  }

  public clean() {
    this.filter = '';
    this.selectedPlayer = undefined;
    this.loadPlayer();
  }

  public scanSuccessHandler($event: string) {
    const playerMatch = /(p|player)\/([0-9]+)\/?$/.exec($event);

    if ((playerMatch == null) || (playerMatch.length !== 3)) {
      console.error(`invalid input: ${$event}`);
      this.toastr.warning(`Invalid QR code, make sure this is a player badge!`);
      return;
    }

    const playerIdString = playerMatch[2];
    const playerId = Number(playerIdString);

    const playerFound = this.playerList.find((player) => player.id === playerId);
    if (playerFound) {
      this.onPlayerFound.emit(playerFound);
    } else {
      this.toastr.warning(`Player with id ${playerId} not found.'`);
    }
  }

  public onClickPlayer($event: MainPlayer) {
    if (this.selectedPlayer === $event) {
      this.onOk();
    } else {
      this.selectedPlayer = $event;
    }
  }

  public onOk(): void {
    if (this.selectedPlayer !== undefined) {
      this.onPlayerFound.emit(this.selectedPlayer);
    }
  }

  private filterList(filter: string): void {
    const result = this.playerList.filter((item, index, array) => {
      if (item.full_name.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
        return true;
      }
      if (!(item.ifpa_info === null) && !(item.ifpa_info.player === null) &&
      item.ifpa_info.player.player_id.indexOf(filter.toLowerCase()) > -1) {
        return true;
      }

      return false;
    });

    this.playerListFiltered = result
      .sort((a, b) => {
        return a.full_name.localeCompare(b.full_name);
      })
      .slice(0, 10);

    if (this.playerListFiltered.indexOf(this.selectedPlayer) === -1) {
      this.selectedPlayer = undefined;
    }
  }
}

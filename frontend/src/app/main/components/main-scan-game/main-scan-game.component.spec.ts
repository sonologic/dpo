import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainScanGameComponent } from './main-scan-game.component';

describe('MainScanGameComponent', () => {
  let component: MainScanGameComponent;
  let fixture: ComponentFixture<MainScanGameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainScanGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScanGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

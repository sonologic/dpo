import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Game } from 'src/app/models/game';
import { GamesService } from 'src/app/services/games.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main-scan-game',
  templateUrl: './main-scan-game.component.html',
  styleUrls: ['./main-scan-game.component.css']
})
export class MainScanGameComponent implements OnInit {

  @Input() visible: boolean;
  @Input() type: 'scan' | 'manual';
  @Output() onGameFound: EventEmitter<Game> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  public loading = false;
  public gameList: Game[] = [];
  public gameListFiltered: Game[] = [];
  public selectedGame: Game;

  public get filter(): string {
    return this._filter;
  }
  public set filter(value: string) {
    this._filter = value;
    this.filterList(value);
  }

  private _filter: string;

  constructor(private gameService: GamesService, private toastr: ToastrService) { }

  ngOnInit() {
    this.gameService
      .getMainGames()
      .subscribe((result) => {
        this.gameList = result;
        this.filter = '';
      });
  }

  public clean(): void {
    // todo
    this.filter = '';
    this.selectedGame = undefined;
  }

  public scanSuccessHandler($event: string) {
    const gameMatch = /(mg|game)\/([0-9]+)\/?$/.exec($event);

    if ( (gameMatch == null) || (gameMatch.length !== 3) ) {
      console.error(`invalid input: ${$event}`);
      this.toastr.warning(`Invalid QR code, make sure this is a game apron card!`);
      return;
    }


    const gameIdString = gameMatch[2];
    const gameId = Number(gameIdString);

    const gameFound = this.gameList.find((game) => game.id === gameId );
    if (gameFound) {
      this.onGameFound.emit(gameFound);
    } else {
      this.toastr.warning(`Game with id ${gameId} not found.'`);
    }
  }

  public onClickGame($event: Game) {
    if (this.selectedGame === $event) {
      this.onOk();
    } else {
      this.selectedGame = $event;
    }
  }

  public onOk(): void {
    if (this.selectedGame !== undefined) {
      this.onGameFound.emit(this.selectedGame);
    }
  }

  private filterList(filter: string): void {
    const result = this.gameList.filter((item, index, array) => {
      if (item.title.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
        return true;
      }
      if (item.number.toString().indexOf(filter.toLowerCase()) > -1) {
        return true;
      }

      return false;
    });

    this.gameListFiltered = result
      .sort((a, b) => {
        return a.title.localeCompare(b.title);
      })
      .slice(0, 10);


    if (this.gameListFiltered.indexOf(this.selectedGame) === -1) {
      this.selectedGame = undefined;
    }
  }
}

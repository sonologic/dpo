import { Component, OnInit, ViewChild } from '@angular/core';
import { MainScore, State } from '../../models/main-score';
import { MainScanPlayerComponent } from '../../components/main-scan-player/main-scan-player.component';
import { MainPlayer } from 'src/app/models/main_player';
import { Game } from 'src/app/models/game';
import { PlayersService } from 'src/app/services/players.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from 'src/app/services/notification.service';
import { MainScanGameComponent } from '../../components/main-scan-game/main-scan-game.component';
import { MainEnterScoreComponent } from '../../components/main-enter-score/main-enter-score.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-main-scoring',
  templateUrl: './main-scoring.component.html',
  styleUrls: ['./main-scoring.component.css']
})
export class MainScoringComponent implements OnInit {
  @ViewChild(MainScanPlayerComponent, { static: true }) scanPlayerComponent: MainScanPlayerComponent;
  @ViewChild(MainScanGameComponent, { static: true }) scanGameComponent: MainScanGameComponent;
  @ViewChild(MainEnterScoreComponent, { static: true }) enterScoreComponent: MainEnterScoreComponent;

  public mainScore: MainScore = new MainScore();
  public showScores: boolean = false;

  constructor(private playersService: PlayersService, private notificationService: NotificationService) { }

  ngOnInit() {
  }

  public reset(): void {
    this.mainScore = new MainScore();
    this.scanPlayerComponent.clean();
    this.scanGameComponent.clean();
    this.enterScoreComponent.clean();
    this.showScores = false;
  }

  public switchMode(inputType: 'manual' | 'scan'): void {
    this.mainScore.inputType = inputType;
  }

  public onPlayerFound($event: MainPlayer) {
    this.mainScore.player = $event;
    //todo checks

    this.mainScore.setState(State.EnterGame);
  }

  public onShowScores() {
    this.showScores = !this.showScores;
  }

  public onGameFound($event: Game) {
    this.mainScore.game = $event;
    //todo checks

    this.mainScore.setState(State.EnterScore);
  }

  public getGameName(url: string): string {
    if (this.scanGameComponent.gameList) {
      let game = this.scanGameComponent.gameList.find(game => game.url == url);
      if (game) {
        return game.title;
      }
    }
  }

  public onScoreEntered($event: { score: number, wildcard: boolean }) {
    const player = _.cloneDeep(this.mainScore.player);

    let wildcardHasBeenPlayed = _.some(this.mainScore.player.scores, (score) => score.wildcard);
    let position = this.mainScore.player.scores.length + (wildcardHasBeenPlayed ? 0 : 1);
   
    if ($event.wildcard) {
      position = 9;
    }

    player.scores.push({
      id: null,
      game: this.mainScore.game.url,
      position: position,
      player: this.mainScore.player.url,
      score: $event.score,
      url: '',
      valid: true,
      wildcard: $event.wildcard
    });

    this.playersService.saveMainPlayer(player)
      .subscribe((result) => {
        this.notificationService.success('Score submitted');
        this.reset();
      }, (error) => {
        this.notificationService.apiError(error);
      });
  }
}

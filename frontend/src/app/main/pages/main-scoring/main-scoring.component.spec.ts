import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainScoringComponent } from './main-scoring.component';

describe('MainScoringComponent', () => {
  let component: MainScoringComponent;
  let fixture: ComponentFixture<MainScoringComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainScoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

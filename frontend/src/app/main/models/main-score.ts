import { Game } from "src/app/models/game";
import { MainPlayer } from "src/app/models/main_player";

export enum State {
    SelectInputType = 0,
    EnterPlayer = 1,
    EnterGame = 2,
    EnterScore = 3,
    Complete = 4
}

export class MainScore {
    private _inputType: 'choice' | 'scan' | 'manual' = 'scan';
    private _state: State = State.EnterPlayer;
    private _player: MainPlayer;
    private _game: Game;
    private _score: number;

    public get inputType(): 'choice' | 'scan' | 'manual' {
        return this._inputType;
    }

    public set inputType(value: 'choice' | 'scan' | 'manual') {
        if (this._state === State.EnterPlayer) { 
            this._inputType = value;
        }
    }

    public get state(): State {
        return this._state;
    }

    public get player(): MainPlayer {
        return this._player;
    }

    public set player(value: MainPlayer) {
        if (this._state === State.EnterPlayer) {
            this._player = value;
        }
    }

    public get game(): Game {
        return this._game;
    }

    public set game(value: Game) {
        if (this._state === State.EnterGame && this.canSetGame(value)) {
            this._game = value;
        }
    }

    public get score(): number {
        return this._score;
    }

    public set score(value: number) {
        if (this._state === State.EnterScore) {
            this._score = value;
        }
    }

    public constructor() {

    }

    public setState(newState: State): void {
        switch (this._state) {
            case State.SelectInputType:
                if (newState === State.EnterPlayer) {
                    this._state = newState;
                    return;
                }
                break;
            case State.EnterPlayer:
                if (newState === State.EnterGame) {
                    this._state = newState;
                    return;
                }
                break;
            case State.EnterGame:
                if (newState === State.EnterScore) {
                    this._state = newState;
                    return;
                }
                break;
            case State.EnterScore:
                break;
        }

        throw (`Invalid statechange ${this._state} -> ${newState}`);
    }

    private canSetGame(game: Game): boolean {
        let found = false;
        return true;
    }
}

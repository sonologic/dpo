import { Component, OnInit } from '@angular/core';
import {Game} from '../models/game';
import {GamesService} from '../services/games.service';
import {GameRankingService} from '../services/game-ranking.service';
import {ActivatedRoute} from '@angular/router';
import {Globals} from '../globals';

@Component({
  selector: 'app-main-game-ranking',
  templateUrl: './main-game-ranking.component.html',
  styleUrls: ['./main-game-ranking.component.css']
})
export class MainGameRankingComponent implements OnInit {

  games: Game[];
  selectedGame: number;

  constructor(
    private gamesService: GamesService,
  ) { }

  ngOnInit() {
    this.gamesService.getMainGames().subscribe(
      games => {
        this.games = games;
        if (games.length > 0) {
          this.selectedGame = games[0].id;
        }
      }
    );
  }

}

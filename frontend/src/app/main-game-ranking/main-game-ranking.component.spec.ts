import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainGameRankingComponent } from './main-game-ranking.component';

describe('MainGameRankingComponent', () => {
  let component: MainGameRankingComponent;
  let fixture: ComponentFixture<MainGameRankingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainGameRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainGameRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayersService } from '../services/players.service';
import { Player } from '../models/player';
import { Globals } from '../globals';
import { TicketsService } from '../services/classics-tickets.service';
import { Ticket } from '../models/ticket';
import { forkJoin } from 'rxjs';
import { GamesService } from '../services/games.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-player-standings',
  templateUrl: './player-standings.component.html',
  styleUrls: ['./player-standings.component.css']
})
export class PlayerStandingsComponent implements OnInit {
  public player: Player;
  public tickets: Ticket[];
  public classicGames: Game[];
  public games: Game[];

  public get hasIfpa(): boolean {
    if (this.player && this.player.number > 0) {
      return true;
    } else {
      return false;
    }
  }

  public get tournamentType(): string {
    return this.globals.tournament.type;
  }

  constructor(private activatedRoute: ActivatedRoute,
    private playersService: PlayersService,
    private ticketService: TicketsService,
    private gamesService: GamesService,
    private globals: Globals) {
  }

  ngOnInit() {
    const playerNumber: number = Number(this.activatedRoute.snapshot.params['playerId']);
    this.loadPlayerDetail(playerNumber);
  }

  public getTicketGames(ticket: Ticket): Game[] {
    let result: Game[] = [];
    ticket.scores.forEach((item) => {
      const findGame = this.classicGames.find((game) => { return game.url == item.game; })
      if (findGame) {
        result.push(findGame);
      }
    });
    return result;
  }

  private loadPlayerDetail(playerId: number): void {
    if (playerId === undefined) {
      //todo err
      return;
    }

    this.playersService.getPlayer(playerId)
      .subscribe(
        player => {
          this.player = player;

          if (this.tournamentType === 'classics') {
            this.loadClasicTickets();
          }
          if (this.tournamentType === 'main') {
            this.loadMain(playerId);
          }
        }
      );
  }

  private loadClasicTickets() {
    forkJoin(this.ticketService.getTicketsByPlayerId(this.player.id), this.gamesService.getClassicsGames())
      .subscribe((result) => {
        this.tickets = result[0];
        this.classicGames = result[1];
      });
  }

  private loadMain(playerId: number){
    forkJoin(this.playersService.getMainPlayerScores(playerId), this.gamesService.getMainGames())
    .subscribe((result) => {
      this.player = result[0];
      this.games = result[1];
    });
  }

  public getGameName(url: string): string {
    if (this.games) {
      let game = this.games.find(game => game.url == url);
      if (game) {
        return game.title;
      }
    }
  }
}

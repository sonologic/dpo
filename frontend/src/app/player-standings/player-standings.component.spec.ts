import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlayerStandingsComponent } from './player-standings.component';

describe('PlayerStandingsComponent', () => {
  let component: PlayerStandingsComponent;
  let fixture: ComponentFixture<PlayerStandingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerStandingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerStandingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { MainPlayer } from '../models/main_player';
import { PlayersService } from '../services/players.service';
import { Globals } from '../globals';
import { Observable, timer } from 'rxjs';
import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-main-player-ranking',
  templateUrl: './main-player-ranking.component.html',
  styleUrls: ['./main-player-ranking.component.css']
})
export class MainPlayerRankingComponent implements OnInit {
  public playerRankings: {
    id: number,
    position: number,
    name: string,
    points: number,
    gamesPlayed: number,
    wildcardPlayed: boolean,
    tag: string,
  }[];

  public players: MainPlayer[];

  private timer: Observable<number>;
  private timerSubscription;
  private wallBoardMode = false;

  constructor(
    private route: ActivatedRoute,
    private playersService: PlayersService,
    private globals: Globals
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        if ('wb' in params) {
          this.wallBoardMode = true;
          this.startRefreshTimer(0);
        } else {
          this.refreshPlayers();
        }
      });
  }

  private startRefreshTimer(initialDelay: number) {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.timer = timer(initialDelay, environment.wallboard_refresh_period_in_seconds * 1000);
    this.timerSubscription = this.timer.subscribe(
      () => {
        this.refreshPlayers();
        if (this.wallBoardMode) {
          this.scrollAnimation();
        }
      }
    );
  }

  private scrollAnimation() {
    const animationDelay = (environment.wallboard_refresh_period_in_seconds / 8) * 1000;

    $('html, body').stop(true, false)
      .delay(animationDelay)
      .animate({ scrollTop: $('html, body').get(0).scrollHeight - $('html, body').get(0).clientHeight }, animationDelay * 5, 'linear')
      .delay(animationDelay)
      .animate({ scrollTop: 0 }, animationDelay * 1);
  }

  private refreshPlayers() {
    this.playersService.getMainPlayers().subscribe(
      players => {
        this.players = players;
        this.playerRankings = [];
        let i = 0;
        _.each(players, (player) => {
          i++;
          this.playerRankings.push({
            id: player.id,
            name: player.full_name,
            points: player.points,
            position: i,
            tag: player.tag,
            gamesPlayed: player.scores.length,
            wildcardPlayed: this.wildcardPlayed(player)
          });
        });
      }
    );
  }

  private wildcardPlayed(player: MainPlayer): boolean {
    let wildcardPlayed = false;
    _.each(player.scores, (score) => {
      if (score.wildcard) {
        wildcardPlayed = true;
      }
    });

    return wildcardPlayed;
  }

  get qualifiers(): number {
    if (this.globals.tournament.qualifiers > this.players.length) {
      return this.players.length;
    }
    return this.globals.tournament.qualifiers;
  }


}

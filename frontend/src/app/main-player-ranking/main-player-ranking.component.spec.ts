import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MainPlayerRankingComponent } from './main-player-ranking.component';

describe('MainPlayerRankingComponent', () => {
  let component: MainPlayerRankingComponent;
  let fixture: ComponentFixture<MainPlayerRankingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPlayerRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPlayerRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

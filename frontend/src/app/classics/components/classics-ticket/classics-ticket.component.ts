import { Component, Input, OnInit } from '@angular/core';

import { Ticket } from '../../../models/ticket';
import { TicketsService } from '../../../services/classics-tickets.service';
import { TicketScore } from '../../../models/ticket-score';
import { environment } from '../../../../environments/environment';
import { PlayersService } from '../../../services/players.service';
import { Player } from '../../../models/player';
import { Globals } from '../../../globals';
import { Game } from '../../../models/game';
import { GamesService } from '../../../services/games.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-classics-ticket',
  templateUrl: './classics-ticket.component.html',
  styleUrls: ['./classics-ticket.component.css']
})
export class ClassicsTicketComponent implements OnInit {
  private _ticket: Ticket;

  @Input() set ticket(ticket: Ticket) {
    this.processTicket(ticket);
  }

  get ticket() {
    return this._ticket;
  }

  get diagnostic() {
    return JSON.stringify(this.ticket);
  }

  get tournamentType() {
    return this.globals.tournament.type;
  }

  public players: Player[];
  public games: Game[];

  constructor(
    private notificationService: NotificationService,
    private classicsGamesService: GamesService,
    private ticketsService: TicketsService,
    private playersService: PlayersService,
    private globals: Globals
  ) { }

  ngOnInit() {
    this.classicsGamesService.getClassicsGames()
      .subscribe(
        games => {
          this.games = games;
          this.playersService.getPlayers()
            .subscribe(
              players => {
                this.players = players;
              }
            );
        });
  }

  private processTicket(ticket: Ticket) {
    this._ticket = ticket;

    const maxTicketPosition = ticket.scores.reduce(
      function (a: number, b: TicketScore) {
        return Math.max(a, b.position);
      }, 0);

    const newScorePositions: number[] = Array.from(
      { length: (environment.scores_per_ticket - maxTicketPosition) },
      (v, k) => k + maxTicketPosition + 1
    );

    for (const position of newScorePositions) {
      ticket.scores.push(
        {
          'id': null,
          'url': null,
          'game': null,
          'score': null,
          'position': position,
          'ticket': ticket.url
        }
      );
    }
  }

  public onSubmit() {
    this.ticketsService.saveTicket(this.ticket)
      .subscribe(
        ticket => this.ticket = ticket,
        error => this.notificationService.apiError(error)
      );
  }
}

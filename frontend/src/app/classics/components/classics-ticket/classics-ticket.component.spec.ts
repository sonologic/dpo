import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsTicketComponent } from './classics-ticket.component';

describe('ClassicsTicketComponent', () => {
  let component: ClassicsTicketComponent;
  let fixture: ComponentFixture<ClassicsTicketComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

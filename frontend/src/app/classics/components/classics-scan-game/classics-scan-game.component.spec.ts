import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsScanGameComponent } from './classics-scan-game.component';

describe('ClassicsScanGameComponent', () => {
  let component: ClassicsScanGameComponent;
  let fixture: ComponentFixture<ClassicsScanGameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsScanGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsScanGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

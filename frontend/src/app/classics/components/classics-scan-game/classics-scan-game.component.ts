import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Game } from 'src/app/models/game';
import { GamesService } from 'src/app/services/games.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-classics-scan-game',
  templateUrl: './classics-scan-game.component.html',
  styleUrls: ['./classics-scan-game.component.css']
})
export class ClassicsScanGameComponent implements OnInit {
  @Input() visible: boolean;
  @Input() type: 'scan' | 'manual';
  @Output() onGameFound: EventEmitter<Game> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  public loading = false;
  public gameNumber: number;

  constructor(private gameService: GamesService,
    private toastr: ToastrService) { }

  ngOnInit() {
  }

  public clean(): void {
    this.gameNumber = null;
  }

  public scanSuccessHandler($event: string) {
    const gameMatch = /(cg|game)\/([0-9]+)\/?$/.exec($event);

    if ( (gameMatch == null) || (gameMatch.length !== 3) ) {
      console.error(`invalid input: ${$event}`);
      this.toastr.warning(`invalid input: ${$event}. Expected QR ending with '(cg|game)/{number}'`);
      return;
    }

    const gameIdString = gameMatch[2];
    const gameId = Number(gameIdString);
    this.loadGame(gameId);
  }

  public manualInput(code: string) {
    const gameId = Number(code);
    this.loadGame(gameId);
  }

  public loadGame(gameId: number) {
    this.loading = true;
    this.gameService.getClassicGameById(gameId)
      .subscribe(
        game => {
          this.onGameFound.emit(game);
        },
        error => {
          if (error.status === 404) {
            console.error('game not found');
            this.toastr.error(`Game with game numer ${gameId} does not exist.`);
          } else {
            console.error(error.message);
          }
          this.loading = false;
        },
        () => {
          this.loading = false;
        }
      );
  }
}

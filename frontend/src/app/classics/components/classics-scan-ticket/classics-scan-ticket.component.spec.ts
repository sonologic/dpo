import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsScanTicketComponent } from './classics-scan-ticket.component';

describe('ClassicsScanTicketComponent', () => {
  let component: ClassicsScanTicketComponent;
  let fixture: ComponentFixture<ClassicsScanTicketComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsScanTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsScanTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

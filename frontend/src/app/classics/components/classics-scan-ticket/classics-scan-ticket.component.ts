import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Ticket } from 'src/app/models/ticket';
import { TicketsService } from 'src/app/services/classics-tickets.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-classics-scan-ticket',
  templateUrl: './classics-scan-ticket.component.html',
  styleUrls: ['./classics-scan-ticket.component.css']
})
export class ClassicsScanTicketComponent implements OnInit, OnChanges {

  @Input() visible: boolean;
  @Input() type: 'scan' | 'manual';
  @Output() onTicketFound: EventEmitter<Ticket> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  public loading = false;
  public ticketNumber: number;

  constructor(private ticketsService: TicketsService,
    private toastr: ToastrService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
  }

  public clean(): void {
    this.ticketNumber = null;
  }

  public scanSuccessHandler($event: string) {
    const ticketMatch = /(c|ticket)\/([0-9]+)\/?$/.exec($event);

    if ( (ticketMatch == null) || (ticketMatch.length !== 3) ) {
      console.error(`invalid input: ${$event}`);
      this.toastr.warning(`invalid input: ${$event}. Expected QR ending with 'ticket/{number}'`);
      return;
    }

    const ticketIdString = ticketMatch[2];
    const ticketId = Number(ticketIdString);
    this.loadTicket(ticketId);
  }

  public manualInput(code: string) {
    const ticketId = Number(code);
    this.loadTicket(ticketId);
  }

  private loadTicket(ticketId: number) {
    this.loading = true;
    this.ticketsService.getTicketByNumber(ticketId)
      .subscribe(
        ticket => {
          this.onTicketFound.emit(ticket);
        },
        error => {
          if (error.status === 404) {
            // not found so new ticket
            this.toastr.info('Ticket does not exist yet. Adding new ticket');
            const ticket = new Ticket();
            ticket.id = ticketId;
            this.onTicketFound.emit(ticket);
          } else {
            console.error(error.message);
            this.toastr.error(error.message);
          }

          this.loading = false;
        }, () => {
          this.loading = false;
        });
  }
}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsNewTicketComponent } from './classics-new-ticket.component';

describe('ClassicsNewTicketComponent', () => {
  let component: ClassicsNewTicketComponent;
  let fixture: ComponentFixture<ClassicsNewTicketComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsNewTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsNewTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

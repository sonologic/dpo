import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayersService } from '../../../services/players.service';
import { Player } from '../../../models/player';
import { TicketsService } from '../../../services/classics-tickets.service';
import { Ticket } from '../../../models/ticket';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-classics-new-ticket',
  templateUrl: './classics-new-ticket.component.html',
  styleUrls: ['./classics-new-ticket.component.css']
})
export class ClassicsNewTicketComponent implements OnInit {
  @Input() visible: boolean;
  @Input() ticketNumber: number;
  @Input() type: 'scan' | 'manual';
  @Output() newTicket = new EventEmitter<Ticket>();

  public playerList: Player[];
  public playerListFiltered: Player[] = [];
  public selectedPlayer: Player = null;
  public playerName: string = null;
  public get filter(): string {
    return this._filter;
  }
  public set filter(value: string) {
    this._filter = value;
    this.filterList(value);
  }

  private _filter: string;

  constructor(
    private playersService: PlayersService,
    private ticketsService: TicketsService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadPlayers();
  }

  public clean(): void {
    this.playerList = null;
    this.playerListFiltered = null;
    this.playerName = null;
    this.selectedPlayer = null;
    this.loadPlayers();
  }

  private loadPlayers(): void {
    this.playersService
      .getPlayers()
      .subscribe((players) => {
        this.playerList = players;
        this.filter = '';
      });
  }

  public scanSuccessHandler($event: string) {
    const playerMatch = /(p|player)\/([0-9]+)\/?$/.exec($event);

    if ((playerMatch == null) || (playerMatch.length !== 3)) {
      console.error(`invalid input: ${$event}`);
      this.toastr.warning(`invalid input: ${$event}. Expected QR ending with '(cg|player)/{number}'`);
      return;
    }

    const playerIdString = playerMatch[2];
    const playerId = Number(playerIdString);

    const player = this.playerList.find((player) => {
      return player.id === playerId;
    });

    if (player === null) {
      this.toastr.warning(`Player with id = ${playerId} not found`);
      return;
    }

    this.selectedPlayer = player;
    if (this.selectedPlayer.activated) {
      this.onSubmit();
    }
  }

  public onSubmit() {
    if (this.selectedPlayer === null) {
      return;
    }

    if (this.selectedPlayer.activated === false && (this.playerName === null || this.playerName.length === 0)) {
      this.toastr.warning('Playername is empy');
      return;
    }

    if (this.selectedPlayer.activated === false) {
      this.updatePlayerName();
    } else {
      this.createTicket();
    }
  }

  private updatePlayerName() {
    this.selectedPlayer.full_name = this.playerName;
    this.selectedPlayer.activated = true;
    this.playersService.savePlayer(this.selectedPlayer)
      .toPromise()
      .then((player) => {
        this.selectedPlayer = player;
        this.createTicket();
      });
  }

  private createTicket() {
    this.ticketsService.createTicket(this.ticketNumber, this.selectedPlayer)
      .toPromise()
      .then((ticket) => {
        this.ticketsService
          .getTicketByNumber(this.ticketNumber)
          .toPromise()
          .then((result) => {
            this.newTicket.emit(result);
          })
          .catch((reason) => {
            this.toastr.error(reason.message);
          });
      })
      .catch((error) => {
        this.toastr.error(error.message);
      });
  }

  public onClickPlayer($event: Player) {
    if (this.selectedPlayer === $event) {
      this.onSubmit();
    } else {
      this.selectedPlayer = $event;
    }
  }

  private filterList(filter: string): void {
    const result = this.playerList.filter((item, index, array) => {
      if (item.full_name.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
        return true;
      }
      if (!(item.number === null) && item.number === Number(filter)) {
        return true;
      }
      if (item.id === Number(filter)) {
        return true;
      }
      return false;
    });

    this.playerListFiltered = result
      .sort((a, b) => {
        return a.full_name.localeCompare(b.full_name);
      })
      .slice(0, 10);

    if (this.playerListFiltered.indexOf(this.selectedPlayer) === -1) {
      this.selectedPlayer = null;
    }
  }
}

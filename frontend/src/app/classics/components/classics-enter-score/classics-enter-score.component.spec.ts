import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsEnterScoreComponent } from './classics-enter-score.component';

describe('ClassicsEnterScoreComponent', () => {
  let component: ClassicsEnterScoreComponent;
  let fixture: ComponentFixture<ClassicsEnterScoreComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsEnterScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsEnterScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

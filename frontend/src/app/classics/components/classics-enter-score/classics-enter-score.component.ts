import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UntypedFormBuilder, ReactiveFormsModule, FormsModule, NgControl, UntypedFormGroup } from '@angular/forms';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-classics-enter-score',
  templateUrl: './classics-enter-score.component.html',
  styleUrls: ['./classics-enter-score.component.css']
})
export class ClassicsEnterScoreComponent implements OnInit {
  @Input() visible: boolean;
  @Output() onScoreEntered: EventEmitter<number> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  @Output() onReset: EventEmitter<void> = new EventEmitter();
  private _score: string;
  public loading = false;
  public form: UntypedFormGroup;
  public visualScore: number;
  public aggreed = false;
  public get score(): string {
    return this._score;
  }

  public set score(value: string) {
    if ((value === null)){
      value = '';
    }
    this._score = formatNumber((<any>value).replace(/\D/g, ''), 'en-EN');
  }

  constructor(formBuilder: UntypedFormBuilder) {
    this.form = formBuilder.group({ score: [''] });
  }

  ngOnInit() {
  }

  public agree(): void {
    this.aggreed = !this.aggreed;
    this.visualScore = parseInt(this.score.toString().replace(/\D/g, ''));
  }

  public clean(): void {
    this.score = null;
    this.aggreed = false;
  }

  public submit(): void {
    let score = parseInt(this.score.toString().replace(/\D/g, ''));
    this.onScoreEntered.emit(score);
  }
}

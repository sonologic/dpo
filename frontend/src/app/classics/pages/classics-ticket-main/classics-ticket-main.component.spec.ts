import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsTicketMainComponent } from './classics-ticket-main.component';

describe('ClassicsTicketMainComponent', () => {
  let component: ClassicsTicketMainComponent;
  let fixture: ComponentFixture<ClassicsTicketMainComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsTicketMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsTicketMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

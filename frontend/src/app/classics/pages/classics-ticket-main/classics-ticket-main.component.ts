import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, ReactiveFormsModule, FormsModule, NgControl, FormGroup } from '@angular/forms';
import { Ticket } from '../../../models/ticket';
import { ClassicScore, State } from '../../models/classicScore';
import { Router } from '@angular/router';
import { Game } from 'src/app/models/game';
import { TicketsService } from 'src/app/services/classics-tickets.service';
import { GamesService } from 'src/app/services/games.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { ClassicsScanTicketComponent } from '../../components/classics-scan-ticket/classics-scan-ticket.component';
import { ClassicsScanGameComponent } from '../../components/classics-scan-game/classics-scan-game.component';
import { ClassicsEnterScoreComponent } from '../../components/classics-enter-score/classics-enter-score.component';
import { ClassicsNewTicketComponent } from '../../components/classics-new-ticket/classics-new-ticket.component';

@Component({
  selector: 'app-classics-ticket-main',
  templateUrl: './classics-ticket-main.component.html',
  styleUrls: ['./classics-ticket-main.component.css']
})
export class ClassicsTicketMainComponent implements OnInit {
  @ViewChild(ClassicsScanTicketComponent, { static: true }) scanTicketComponent: ClassicsScanTicketComponent;
  @ViewChild(ClassicsScanGameComponent, { static: true }) scanGameComponent: ClassicsScanGameComponent;
  @ViewChild(ClassicsEnterScoreComponent, { static: true }) enterScoreComponent: ClassicsEnterScoreComponent;
  @ViewChild(ClassicsNewTicketComponent, { static: true }) newTicketComponent: ClassicsNewTicketComponent;

  private classicGames: Game[];
  public classicScore: ClassicScore = new ClassicScore();
  public createTicket = false;
  public ticketNumber: number;
  public askSure = false;

  constructor(
    private router: Router,
    formBuilder: UntypedFormBuilder,
    private ticketService: TicketsService,
    private gamesService: GamesService,
    private toastr: ToastrService
  ) {

  }

  ngOnInit() {
    this.loadGames();
  }

  public reset() {
    this.classicScore = new ClassicScore();
    this.scanTicketComponent.clean();
    this.scanGameComponent.clean();
    this.enterScoreComponent.clean();
    this.newTicketComponent.clean();
    this.askSure = false;
  }

  public get hasIfpa(): boolean {
    if (this.classicScore.player && this.classicScore.player.number > 0) {
      return true;
    } else {
      return false;
    }
  }
  public onVoid() {
    this.classicScore.ticket.void = true;
    this.ticketService.saveTicket(this.classicScore.ticket)
      .toPromise()
      .then(() => {
        this.toastr.success('Ticket voided');
        this.reset();
       })
      .catch((reason) => {
        this.toastr.error(reason.message)
      });
  }

  public onInputTypeChange(type: 'manual' | 'scan') {
    this.classicScore.inputType = type;
  }

  public onSelectInputType(inputType: 'scan' | 'manual') {
    this.classicScore.inputType = inputType;
    this.classicScore.setState(State.EnterTicket);
  }

  public onTicketFound($event: Ticket): void {
    this.classicScore.ticket = $event;
    this.ticketNumber = $event.id;
    if (this.classicScore.ticket.scores.length === 3) {
      this.classicScore.setState(State.Complete);
      return;
    }

    if ($event.player_details === undefined) {
      this.classicScore.setState(State.EnterPlayer);
    } else {
      this.classicScore.player = $event.player_details;
      this.classicScore.setState(State.EnterGame);
    }
  }

  public onGameFound($event: Game): void {
    this.classicScore.game = $event;
    if (this.classicScore.game == $event) {
      this.classicScore.setState(State.EnterScore);
    } else {
      this.toastr.warning(`Game ${$event.title} already played`);
    }
  }

  public onScoreEntered($event: number): void {
    const restore = _.cloneDeep(this.classicScore);

    this.classicScore.score = $event;
    if (this.classicScore.ticket.scores.length != 3) {
      this.classicScore.ticket.scores.push({
        game: this.classicScore.game.url,
        position: this.classicScore.ticket.scores.length + 1,
        score: this.classicScore.score,
        ticket: this.classicScore.ticket.url,
        id: null,
        url: null
      })

      this.ticketService.saveTicket(this.classicScore.ticket)
        .toPromise()
        .then(() => {
          console.log('DONE');
          this.reset();
          this.toastr.success('Scores are saved.')
        })
        .catch((reason) => {
          console.error(reason.message);
          this.toastr.error(reason.message)
          this.classicScore = restore;
        });
    } else {

    }
  }

  /**
   * Get the classic game name using the url
   * @param gameUrl Url to the classic game
   */
  public gameName(gameUrl: string): string {
    if (this.classicGames == undefined) {
      return '?';
    }

    let game = this.classicGames.find((game) => { return game.url === gameUrl; });
    if (game == undefined) {
      return '?';
    }

    return game.title;
  }

  /**
   * Load the classics
   */
  private loadGames(): void {
    this.gamesService.getClassicsGames()
      .subscribe((games) => {
        this.classicGames = games;
      });
  }
}

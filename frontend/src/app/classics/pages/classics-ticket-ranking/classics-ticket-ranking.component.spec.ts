import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsTicketRankingComponent } from './classics-ticket-ranking.component';

describe('ClassicsTicketRankingComponent', () => {
  let component: ClassicsTicketRankingComponent;
  let fixture: ComponentFixture<ClassicsTicketRankingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsTicketRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsTicketRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';
import {Globals} from '../../../globals';
import {TicketsService} from '../../../services/classics-tickets.service';
import {Ticket} from '../../../models/ticket';
import {Observable, timer} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-classics-ticket-ranking',
  templateUrl: './classics-ticket-ranking.component.html',
  styleUrls: ['./classics-ticket-ranking.component.css']
})
export class ClassicsTicketRankingComponent implements OnInit {
  tickets: Ticket[];
  lastQualifyingRow: number;

  private timer: Observable<number>;
  private timerSubscription;

  private wallBoardMode = false;

  constructor(
    private ticketService: TicketsService,
    private route: ActivatedRoute,
    private globals: Globals
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        if('wb' in params) {
          this.wallBoardMode = true;
          this.startRefreshTimer(0);
        } else {
          this.refreshTickets();
        }
      });
  }

  startRefreshTimer(initialDelay: number) {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.timer = timer(initialDelay, environment.wallboard_refresh_period_in_seconds * 1000);
    this.timerSubscription = this.timer.subscribe(
      () => {
        this.refreshTickets();
      }
    );
  }

  refreshTickets() {
    this.ticketService.getTickets()
      .subscribe(
        tickets => {
          this.tickets = tickets;
          this.lastQualifyingRow = this.determineLastQualifyingRow();
          if (this.wallBoardMode) {
            this.scrollAnimation();
          }
        }
      );
  }

  scrollAnimation() {
    const animationDelay = Math.floor((environment.wallboard_refresh_period_in_seconds / 8) * 1000);

    $('html, body').stop(true, false)
      .delay(animationDelay)
      .animate({ scrollTop: $('html, body').get(0).scrollHeight - $('html, body').get(0).clientHeight }, animationDelay * 5, 'linear')
      .delay(animationDelay)
      .animate({ scrollTop: 0 }, animationDelay * 1, 'linear');
  }

  get showTicketNumber() {
    return this.globals.tournament.type === 'classics';
  }

  determineLastQualifyingRow(): number {
    const names: Array<string> = [];

    let rowIndex = 0;

    for (const ticket of this.tickets) {
      const full_name = ticket.player_details.full_name;
      if (!names.includes(full_name)) {
        names.push(full_name);
      }
      if (names.length === this.globals.tournament.qualifiers) {
        return rowIndex;
      }
      rowIndex++;
    }
    return this.tickets.length - 1;
  }

}

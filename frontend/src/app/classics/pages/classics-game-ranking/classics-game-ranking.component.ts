import {Component, OnInit} from '@angular/core';
import {Game} from '../../../models/game';
import {GamesService} from '../../../services/games.service';

@Component({
  selector: 'app-classics-game-ranking',
  templateUrl: './classics-game-ranking.component.html',
  styleUrls: ['./classics-game-ranking.component.css']
})
export class ClassicsGameRankingComponent implements OnInit {

  games: Game[];
  selectedGame: number;

  constructor(
    private gamesService: GamesService,
  ) { }

  ngOnInit() {
    this.gamesService.getClassicsGames().subscribe(
      games => {
        this.games = games;
        if (games.length > 0) {
          this.selectedGame = games[0].id;
        }
      }
    );
  }

}

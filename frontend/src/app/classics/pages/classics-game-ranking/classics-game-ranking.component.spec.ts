import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClassicsGameRankingComponent } from './classics-game-ranking.component';

describe('ClassicsGameRankingComponent', () => {
  let component: ClassicsGameRankingComponent;
  let fixture: ComponentFixture<ClassicsGameRankingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicsGameRankingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicsGameRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Ticket } from "src/app/models/ticket";
import { Player } from "src/app/models/player";
import { Game } from "src/app/models/game";
import * as _ from 'lodash';

export enum State {
    SelectInputType = 0,
    EnterTicket = 1,
    EnterPlayer = 2,
    EnterGame = 3,
    EnterScore = 4,
    Complete = 5
};

export class ClassicScore {
    private _inputType: 'scan' | 'manual' = 'scan';
    private _state: State = State.EnterTicket;
    private _ticket: Ticket;
    private _player: Player;
    private _game: Game;
    private _score: number;

    public get inputType(): 'scan' | 'manual' {
        return this._inputType;
    }

    public get state(): State {
        return this._state;
    }

    public get ticket(): Ticket {
        return this._ticket;
    }

    public set ticket(value: Ticket) {
        if (this._state === State.EnterTicket || this._state === State.EnterPlayer) {
            this._ticket = value;
        }
    }

    public get player(): Player {
        return this._player;
    }

    public set inputType(value: 'scan' | 'manual') {
        if (this._state === State.EnterTicket) {
            this._inputType = value;
        }
    }

    public set player(value: Player) {
        if (this._state === State.EnterPlayer || this._state === State.EnterTicket) {
            this._player = value;
        }
    }

    public get game(): Game {
        return this._game;
    }

    public set game(value: Game) {
        if (this._state === State.EnterGame && this.canSetGame(value)) {
            this._game = value;
        }
    }

    public get score(): number {
        return this._score;
    }

    public set score(value: number) {
        if (this._state === State.EnterScore) {
            this._score = value;
        }
    }

    public constructor() {
     
    }

    public setState(newState: State): void {
        switch (this._state) {
            case State.SelectInputType:
                if (newState === State.EnterTicket) {
                    this._state = newState;
                    return;
                }
                break;
            case State.EnterTicket:
                if (newState === State.EnterPlayer || newState === State.EnterGame || newState === State.Complete) {
                    this._state = newState;
                    return;
                }
                break;

            case State.EnterPlayer:
                if (newState === State.EnterGame) {
                    this._state = newState;
                    return;
                }
                break;

            case State.EnterGame:
                if (newState === State.EnterScore) {
                    this._state = newState;
                    return;
                }
                break;
        }

        throw (`Invalid statechange ${this._state} -> ${newState}`);
    }

    private canSetGame(game: Game): boolean {
        let found = false;
        _.each(this.ticket.scores, (score) => {
            if (score.game == game.url) {
                found = true;
            }
        });

        return !found;
    }
}

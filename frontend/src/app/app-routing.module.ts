import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LogonComponent } from './logon/logon.component';

// Classics
import { ClassicsGameRankingComponent } from './classics/pages/classics-game-ranking/classics-game-ranking.component';
import { ClassicsTicketMainComponent } from './classics/pages/classics-ticket-main/classics-ticket-main.component';
import { ClassicsTicketRankingComponent } from './classics/pages/classics-ticket-ranking/classics-ticket-ranking.component';

// Main event
import { MainPlayerScoreComponent } from './main-player-score/main-player-score.component';
import { MainPlayerComponent } from './main-player/main-player.component';
import { MainPlayerRankingComponent } from './main-player-ranking/main-player-ranking.component';

// Shared components
import { PlayerStandingsComponent } from './player-standings/player-standings.component';
import { GameRankingComponent } from './game-ranking/game-ranking.component';

// Admin
import { AdminComponent } from './admin/admin.component';
import { PlayerimportComponent } from './admin/pages/playerimport/playerimport.component'

import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { from } from 'rxjs';
import { MainScoringComponent } from './main/pages/main-scoring/main-scoring.component';

const routes: Routes = [
  { path: '', redirectTo: '/main/standing', pathMatch: 'full' },
  { path: 'main', redirectTo: '/main/standing', pathMatch: 'full' },
  { path: 'logon', component: LogonComponent },

  { path: 'classics', redirectTo: '/classics/standing', pathMatch: 'full' },
  { path: 'classics/game/:gameId', component: ClassicsGameRankingComponent },
  { path: 'classics/games', component: GameRankingComponent },
  { path: 'classics/player/:playerId', component: PlayerStandingsComponent },
  { path: 'classics/standing', component: ClassicsTicketRankingComponent },
  { path: 'classics/ticket', component: ClassicsTicketMainComponent, canActivate: [AuthGuard] },

  { path: 'main/games', component: GameRankingComponent },
  // ols { path: 'main/player', component: MainPlayerComponent, canActivate: [AuthGuard] },
  { path: 'main/player', component: MainScoringComponent, canActivate: [AuthGuard] },

  { path: 'main/player/:playerId', component: PlayerStandingsComponent },
  { path: 'main/player/:playerId/score', component: MainPlayerScoreComponent, canActivate: [AuthGuard] },
  { path: 'main/standing', component: MainPlayerRankingComponent },
  { path: 'main/standing/player/:playerId', component: PlayerStandingsComponent },

  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'admin/playerimport', component: PlayerimportComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

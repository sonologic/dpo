import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tournament} from '../models/tournament';
import {Globals} from '../globals';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.css']
})
export class TournamentComponent implements OnInit {

  selected_tournament: Tournament;

  @Output() tournament_selected = new EventEmitter<Tournament>();

  constructor(
    private globals: Globals,
    private router: Router,
  ) { }

  ngOnInit() {
    if(!this.globals.tournament) {
      this.globals.tournament = Tournament.tournaments[0];
    }
    this.selected_tournament = this.globals.tournament;
    this.router.events.subscribe(
      event => {
        if (event instanceof NavigationEnd) {
          this.updateTournamentFromNavigation(event.url);
        }
      }
    );
  }

  private updateTournamentFromNavigation(url: string) {
    for(const tournament of Tournament.tournaments) {
      if (url.startsWith('/' + tournament.type + '/')) {
        if(tournament.id !== this.globals.tournament.id) {
          this.globals.tournament = tournament;
          this.selected_tournament = this.globals.tournament;
        }
      }
    }
  }

  get tournaments(): Tournament[] {
    return Tournament.tournaments;
  }

  selectTournament(tournament: Tournament) {
    this.selected_tournament = tournament;
    this.tournament_selected.emit(tournament);
  }

}

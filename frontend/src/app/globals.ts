import { Injectable } from '@angular/core';
import { Tournament } from './models/tournament';

@Injectable()
export class Globals {
  set tournament(tournament: Tournament) {
    sessionStorage.setItem('dpo_tournament', JSON.stringify(tournament));
  }

  get tournament(): Tournament {
    const item = sessionStorage.getItem('dpo_tournament');
    if (item) {
      return JSON.parse(item);
    }
    return null;
  }
}

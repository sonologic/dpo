import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

interface Credentials {
  username: string;
  password: string;
}

@Component({
  selector: 'app-logon',
  templateUrl: './logon.component.html',
  styleUrls: ['./logon.component.css']
})
export class LogonComponent implements OnInit {
  private showLoginForm = false;
  private isLoading = false;
  private loginError = false;

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  onLogin(credentials: Credentials) {
    this.isLoading = true;
    this.loginError = false;
    this.auth.login(credentials).then(
      () => {
        this.isLoading = false;
      },
      () => {
        this.loginError = true;
        this.isLoading = false;
      }
    );
  }

  onLogoff() {
    this.auth.logoff();
    this.showLoginForm = false;
  }

  loggedIn() {
    return this.auth.loggedIn();
  }

    get username() {
    return this.auth.decodedToken.username;
  }

  get showButton() {
    return !this.isLoading;
  }

  get loginFailed(): boolean {
    return this.loginError;
  }

}

import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  get api_url() {
    return environment.api_url;
  }

  get badges_url() {
    const url = new URL('/player/badges/', environment.api_url);
    return url.toString();
  }
}

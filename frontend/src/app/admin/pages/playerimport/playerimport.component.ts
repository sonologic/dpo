import { Component, OnInit } from '@angular/core';
import { PlayersService } from 'src/app/services/players.service';
import { MainPlayer } from 'src/app/models/main_player';

@Component({
  selector: 'app-playerimport',
  templateUrl: './playerimport.component.html',
  styleUrls: ['./playerimport.component.css']
})
export class PlayerimportComponent implements OnInit {

  public players: {
    id: number;
    imported: boolean;
    errors: string;
    voornaam: string;
    tussenvoegsel: string;
    achternaam: string;
    email: string;
    land: string;
    ifpanummer: string;
  }[] = [];
  constructor(private playerService: PlayersService) { }

  ngOnInit() {
  }

  public onReset(): void {
    this.players = [];
  }

  public onSubmit(): void {
    this.players.forEach((player) => {
      let ifpanummer: number;
      ifpanummer = Number(player.ifpanummer);
      const mainPlayer: MainPlayer = {
        activated: true,
        full_name: [player.voornaam, player.tussenvoegsel, player.achternaam].join(' '),
        country_code: player.land,
        id: null,
        ifpa_info: null,
        image: '',
        number: ifpanummer > 0 ? ifpanummer : null,
        points: null,
        scores: null,
        tag: '',
        url: null
      };
      this.playerService.createMainPlayer(mainPlayer)
        .subscribe((a) => {
          player.id = a.id;
          player.imported = true;
        }, (error) => {
          player.errors = this.formatError(error);
        });
    });
  }

  formatError(error) {
    console.log(error);
    const errorMessages: String[] = [];

    for (const key in error.error) {
      console.log(key);
      if (key === 'non_field_errors') {
        errorMessages.push(error.error[key]);
      } else if (key === 'detail') {
        errorMessages.push(error.error[key]);
      } else {
        errorMessages.push(key + ': ' + error.error[key]);
      }
    }

    if (errorMessages.length === 0) {
      errorMessages.push('Unknown error');
    }
    return errorMessages.join('\n');
  }

  public onReadFile($event): void {
    // read file from input
    const fileReaded = $event.target.files[0];
    this.players = [];

    const reader: FileReader = new FileReader();
    reader.readAsText(fileReaded);

    reader.onload = (e) => {
      const csv: string = reader.result as string;
      const allTextLines = csv.split(/\r|\n|\r/);
      const headers = allTextLines[0].split(',');

      for (let i = 1; i < allTextLines.length; i++) {
        // split content based on comma
        const data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          this.players.push({
            id: null,
            imported: false,
            errors: null,
            voornaam: data[0],
            tussenvoegsel: data[1],
            achternaam: data[2],
            email: data[3],
            land: data[4],
            ifpanummer: data[5]
          });
        }
      }
    };
  }
}

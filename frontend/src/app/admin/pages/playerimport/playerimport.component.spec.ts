import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlayerimportComponent } from './playerimport.component';

describe('PlayerimportComponent', () => {
  let component: PlayerimportComponent;
  let fixture: ComponentFixture<PlayerimportComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerimportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerimportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {Tournament} from './models/tournament';
import {Globals} from './globals';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';

  constructor(
    private globals: Globals,
    private authService: AuthService,
    private router: Router,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle('DPO live scores');
  }

  tournamentSelected(tournament: Tournament) {
    this.globals.tournament = tournament;
    this.router.navigateByUrl('/' + this.tournament_type);
  }

  get tournament_type() {
    if (this.globals.tournament) {
      return this.globals.tournament.type;
    } else {
      return null;
    }
  }

  finals_url(type: string) {
    return Tournament.tournamentByType(type).finals_url;
  }

  get is_authenticated() {
    return this.authService.loggedIn();
  }

  get is_staff() {
    return this.authService.loggedIn() && this.authService.decodedToken.is_staff;
  }
}

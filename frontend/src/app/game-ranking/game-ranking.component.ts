import { Component, OnInit } from '@angular/core';
import {Globals} from '../globals';

@Component({
  selector: 'app-game-ranking',
  templateUrl: './game-ranking.component.html',
  styleUrls: ['./game-ranking.component.css']
})
export class GameRankingComponent implements OnInit {

  constructor(
    private globals: Globals,
  ) { }

  ngOnInit() {
  }

  get tournamentType() {
    return this.globals.tournament.type;
  }
}

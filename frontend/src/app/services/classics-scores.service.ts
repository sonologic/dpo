import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClassicsScoresService {

  private games_url = environment.api_url + 'classics/games/';

  constructor(private http: HttpClient) { }

}

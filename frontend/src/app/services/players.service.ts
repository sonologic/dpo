import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Player } from '../models/player';
import { MainPlayer } from '../models/main_player';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  private players_url = environment.api_url + 'players/';
  private main_players_url = environment.api_url + 'main/player/';

  constructor(private http: HttpClient) { }

  /** Load all players */
  public getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.players_url);
  }

  /** Load single player by id */
  public getPlayer(id: number): Observable<Player> {
    // TODO deze bestaat nog niet.
    return this.http.get<Player>(`${this.players_url}${id}`);
  }

  public savePlayer(player: Player): Observable<Player> {
    return this.http.put<Player>(`${this.players_url}${player.id}/`, player);
  }
  
  public getMainPlayers(orderField = '-points'): Observable<MainPlayer[]> {
    const url = this.main_players_url + '?order=' + orderField;
    return this.http.get<MainPlayer[]>(url);
  }

  public getMainPlayerScores(id: number): Observable<MainPlayer> {
    const url = this.main_players_url + id.toString() + '/';
    return this.http.get<MainPlayer>(url);
  }

  public saveMainPlayer(mainPlayer: MainPlayer): Observable<MainPlayer> {
    return this.http.put<MainPlayer>(mainPlayer.url, mainPlayer);
  }

  public createMainPlayer(mainPlayer: MainPlayer): Observable<MainPlayer> {
    return this.http.post<MainPlayer>(this.main_players_url, mainPlayer);
  }
}

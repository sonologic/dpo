import { TestBed, inject } from '@angular/core/testing';

import { TicketsService } from './classics-tickets.service';

describe('ClassicsTicketsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketsService]
    });
  });

  it('should be created', inject([TicketsService], (service: TicketsService) => {
    expect(service).toBeTruthy();
  }));
});

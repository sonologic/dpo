import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Ticket } from '../models/ticket';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private http: HttpClient) { }

  public getTicketByNumber(ticketNumber: number): Observable<Ticket> {
    const url = `${environment.api_url}classics/ticket/number/${ticketNumber.toString()}/`;
    return this.http.get<Ticket>(url);
  }

  public saveTicket(ticket: Ticket): Observable<Ticket> {
    return this.http.put<Ticket>(ticket.url, ticket);
  }

  public getTickets(): Observable<Ticket[]> {
    const url = `${environment.api_url}classics/ticket/?order=-points&valid`;
    return this.http.get<Ticket[]>(url);
  }

  public getTicketsByPlayerId(id: number): Observable<Ticket[]> {
    const url = `${environment.api_url}main/player/${id}/tickets`;
    return this.http.get<Ticket[]>(url);
  }

  public createTicket(number: number, player: Player): Observable<Ticket> {
    const url = `${environment.api_url}classics/ticket/`;
    const data = { 'number': number, 'player': player.url };

    return this.http.post<Ticket>(url, data);
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { ClassicsTicketScoresService } from './classics-scores.service';

describe('ClassicsTicketScoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassicsTicketScoresService]
    });
  });

  it('should be created', inject([ClassicsTicketScoresService], (service: ClassicsTicketScoresService) => {
    expect(service).toBeTruthy();
  }));
});

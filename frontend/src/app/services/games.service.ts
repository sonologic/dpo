import { Injectable } from '@angular/core';
import { Game } from '../models/game';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GamesService {
  private classics_games_url = environment.api_url + 'classics/games/';
  private main_games_url = environment.api_url + 'main/games/';

  private classicsGameCache = new Map();

  constructor(private http: HttpClient) { }

  public getClassicsGames(): Observable<Game[]> {
    const gamesFromCache = this.classicsGameCache.get(URL);
    if (gamesFromCache) {
      return of(gamesFromCache);
    }
    
    const response = this.http.get<Game[]>(this.classics_games_url);
    response.subscribe(beers => this.classicsGameCache.set(URL, beers));
    return response;

    // return this.http.get<Game[]>(this.classics_games_url);
  }

  public getClassicGameById(gameId: number): Observable<Game>{
      return this.http.get<Game>(`${this.classics_games_url}${gameId}`);
  }

  public getMainGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.main_games_url);
  }
}

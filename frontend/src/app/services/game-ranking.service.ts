import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {GameRanking} from '../models/game_ranking';

@Injectable({
  providedIn: 'root'
})
export class GameRankingService {

  private classics_game_ranking_url = environment.api_url + 'classics/ranking/';
  private main_game_ranking_url = environment.api_url + 'main/ranking/';

  constructor(private http: HttpClient) { }

  getGameRankings(): Observable<GameRanking[]> {
    return this.http.get<GameRanking[]>(this.classics_game_ranking_url);
  }

  getClassicsGameRanking(id: number): Observable<GameRanking> {
    return this.http.get<GameRanking>(this.classics_game_ranking_url + id.toString() + '/');
  }

  getMainGameRanking(id: number): Observable<GameRanking> {
    return this.http.get<GameRanking>(this.main_game_ranking_url + id.toString() + '/');
  }
}

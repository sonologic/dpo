import { TestBed, inject } from '@angular/core/testing';

import { GameRankingService } from './game-ranking.service';

describe('GameRankingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameRankingService]
    });
  });

  it('should be created', inject([GameRankingService], (service: GameRankingService) => {
    expect(service).toBeTruthy();
  }));
});

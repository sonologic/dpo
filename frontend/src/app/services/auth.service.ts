import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject, timer} from 'rxjs';
import {environment} from '../../environments/environment';
import {Token} from '../models/token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtHelper: JwtHelperService = new JwtHelperService();

  public loginSubject: Subject<Boolean> = new Subject<Boolean>();

  private timer: Observable<number>;
  private timerSubscription;

  constructor(private http: HttpClient) {
    if (this.loggedIn()) {
      this.startTokenRefreshTimer(0);
    } else {
      localStorage.removeItem('token');
    }
  }

  private startTokenRefreshTimer(initialDelay: number) {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.timer = timer(initialDelay, environment.token_refresh_period_in_seconds * 1000);
    this.timerSubscription = this.timer.subscribe(
      () => {
        this.refreshToken();
      }
    );
  }

  login(credentials) {
    return new Promise((resolve, reject) => {
      this.http.post<Token>(environment.api_url + 'jwt/auth/', credentials)
            .subscribe(
                data => {
                  localStorage.setItem('token', data.token);
                  if (this.loggedIn()) {
                    this.loginSubject.next(true);
                    this.startTokenRefreshTimer(environment.token_refresh_period_in_seconds * 1000);
                    resolve(this.decodedToken);
                  } else {
                    this.loginSubject.next(false);
                    reject({'non_field_errors': ['Invalid token received from server']});
                  }
                },
                error => {
                  this.loginSubject.next(false);
                  reject(error.error);
                }
            );
    });
  }

  refreshToken() {
    const postData = { 'token': localStorage.getItem('token') };
    this.http.post<Token>(environment.api_url + 'jwt/refresh/', postData).subscribe(
      data => {
        localStorage.setItem('token', data.token);
        this.loginSubject.next(true);
      },
      error => {
        this.loginSubject.next(false);
      }
    );
  }

  logoff() {
    localStorage.removeItem('token');
  }

  loggedIn(): boolean {
    let expired = true;
    try {
      expired = this.jwtHelper.isTokenExpired(localStorage.getItem('token'));
    } catch (e) {
      if (e.message !== 'JWT must have 3 parts' &&
          e.message !== 'Cannot decode the token' &&
          !(e instanceof SyntaxError)) {
          console.log(e);
      }
    }
    return !expired;
  }

  get decodedToken() {
    return this.jwtHelper.decodeToken(localStorage.getItem('token'));
  }

}

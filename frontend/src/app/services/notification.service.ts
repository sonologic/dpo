import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private toastr: ToastrService
  ) { }

  private formatError(error) {
    console.log(error);
    const errorMessages: String[] = [];

    for (const key in error.error) {
      console.log(key);
      if (key === 'non_field_errors') {
        errorMessages.push(error.error[key]);
      } else if (key === 'detail') {
        errorMessages.push(error.error[key]);
      } else {
        errorMessages.push(key + ': ' + error.error[key]);
      }
    }

    if (errorMessages.length === 0) {
      errorMessages.push('Unknown error');
    }
    return errorMessages.join('\n');
  }

  public apiError(error) {
    this.toastr.error(this.formatError(error), 'error');
    // this.notifierService.notify('error', this.formatError(error));
  }

  public success(message: string) {
    this.toastr.success(message);
    // this.notifierService.notify('success', message);
  }
}

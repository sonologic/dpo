import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, Toast } from 'ngx-toastr';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { ClassicsTicketComponent } from './classics/components/classics-ticket/classics-ticket.component';
import { ScoreEntryComponent } from './score-entry/score-entry.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { LogonComponent } from './logon/logon.component';
import { TournamentComponent } from './tournament/tournament.component';
import { Globals } from './globals';
import { ClassicsGameRankingComponent } from './classics/pages/classics-game-ranking/classics-game-ranking.component';
import { ClassicsTicketRankingComponent } from './classics/pages/classics-ticket-ranking/classics-ticket-ranking.component';
import { MainPlayerScoreComponent } from './main-player-score/main-player-score.component';
import { MainPlayerComponent } from './main-player/main-player.component';
import { MainPlayerRankingComponent } from './main-player-ranking/main-player-ranking.component';
import { MainGameRankingComponent } from './main-game-ranking/main-game-ranking.component';
import { GameRankingComponent } from './game-ranking/game-ranking.component';
import { GameRankingTableComponent } from './game-ranking-table/game-ranking-table.component';
import { environment } from '../environments/environment';
import { ClassicsNewTicketComponent } from './classics/components/classics-new-ticket/classics-new-ticket.component';
import { ClassicsTicketMainComponent } from './classics/pages/classics-ticket-main/classics-ticket-main.component';
import { PlayerStandingsComponent } from './player-standings/player-standings.component';
import { PlayerInformationIfpaComponent } from './player-information-ifpa/player-information-ifpa.component';
import { ImageDirective } from './image.directive';
import { PlayerClassicsTicketComponent } from './player-classics-ticket/player-classics-ticket.component';
import { PlayerInformationComponent } from './player-information/player-information.component';
import { AdminComponent } from './admin/admin.component';

import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ScanPlayerComponent } from './components/scan-player/scan-player.component';
import { ClassicsScanTicketComponent } from './classics/components/classics-scan-ticket/classics-scan-ticket.component';
import { ClassicsScanGameComponent } from './classics/components/classics-scan-game/classics-scan-game.component';
import { ScoreNumericInputDirective } from './components/score-numeric-input.directive';
import { ClassicsEnterScoreComponent } from './classics/components/classics-enter-score/classics-enter-score.component';
import { PlayerimportComponent } from './admin/pages/playerimport/playerimport.component';
import { MainScoringComponent } from './main/pages/main-scoring/main-scoring.component';
import { MainScanPlayerComponent } from './main/components/main-scan-player/main-scan-player.component';
import { MainScanGameComponent } from './main/components/main-scan-game/main-scan-game.component';
import { MainEnterScoreComponent } from './main/components/main-enter-score/main-enter-score.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    ScoreNumericInputDirective,
    AppComponent,
    ClassicsTicketComponent,
    ScoreEntryComponent,
    LogonComponent,
    TournamentComponent,
    ClassicsGameRankingComponent,
    ClassicsTicketRankingComponent,
    MainPlayerScoreComponent,
    MainPlayerComponent,
    MainPlayerRankingComponent,
    MainGameRankingComponent,
    GameRankingComponent,
    GameRankingTableComponent,
    ClassicsNewTicketComponent,
    ClassicsTicketMainComponent,
    PlayerStandingsComponent,
    PlayerInformationIfpaComponent,
    ImageDirective,
    PlayerClassicsTicketComponent,
    PlayerInformationComponent,
    AdminComponent,
    ScanPlayerComponent,
    ClassicsScanTicketComponent,
    ClassicsScanGameComponent,
    ScoreNumericInputDirective,
    ClassicsEnterScoreComponent,
    PlayerimportComponent,
    MainScoringComponent,
    MainScanPlayerComponent,
    MainScanGameComponent,
    MainEnterScoreComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.jwt_whitelist,
        blacklistedRoutes: []
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ZXingScannerModule
  ],
  exports: [
    ScoreNumericInputDirective
  ],
  providers: [
    Title,
    Globals,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

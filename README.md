# DPO scorekeeping system

The codebase is split in two:

* backend/ - a [https://www.django-rest-framework.org/](Django Rest Framework) API server
* frontend/ - an [https://angular.io/](Angular) SPA front-end

# Frontend

See [frontend/README.md](frontend/README.md) for more information on how to install the front-end.

# Backend

See [backend/README.md](backend/README.md) for more information on how to install the back-end.

